package tech.lp2p.halo.core

internal enum class Type {
    FID, RAW
}
