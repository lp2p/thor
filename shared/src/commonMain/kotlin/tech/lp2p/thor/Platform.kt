package tech.lp2p.thor

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.room.RoomDatabase
import androidx.room.RoomDatabaseConstructor
import androidx.sqlite.driver.bundled.BundledSQLiteDriver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import okio.Path.Companion.toPath
import tech.lp2p.thor.data.Bookmarks
import tech.lp2p.thor.data.Peers
import tech.lp2p.thor.data.Tasks


// The Room compiler generates the `actual` implementations.
@Suppress("NO_ACTUAL_FOR_EXPECT")
expect object TasksConstructor : RoomDatabaseConstructor<Tasks> {
    override fun initialize(): Tasks
}

@Suppress("NO_ACTUAL_FOR_EXPECT")
expect object BookmarksConstructor : RoomDatabaseConstructor<Bookmarks> {
    override fun initialize(): Bookmarks
}


@Suppress("NO_ACTUAL_FOR_EXPECT")
expect object PeersConstructor : RoomDatabaseConstructor<Peers> {
    override fun initialize(): Peers
}

fun createDataStore(producePath: () -> String): DataStore<Preferences> =
    PreferenceDataStoreFactory.createWithPath(
        produceFile = { producePath().toPath() }
    )

internal const val dataStoreFileName = "settings.preferences_pb"


fun peersDatabaseBuilder(
    builder: RoomDatabase.Builder<Peers>
): Peers {
    return builder
        .fallbackToDestructiveMigrationOnDowngrade(true)
        .setDriver(BundledSQLiteDriver())
        .setQueryCoroutineContext(Dispatchers.IO)
        .build()
}


fun tasksDatabaseBuilder(
    builder: RoomDatabase.Builder<Tasks>
): Tasks {
    return builder
        .fallbackToDestructiveMigrationOnDowngrade(true)
        .setDriver(BundledSQLiteDriver())
        .setQueryCoroutineContext(Dispatchers.IO)
        .build()
}


fun bookmarksDatabaseBuilder(
    builder: RoomDatabase.Builder<Bookmarks>
): Bookmarks {
    return builder
        .fallbackToDestructiveMigrationOnDowngrade(true)
        .setDriver(BundledSQLiteDriver())
        .setQueryCoroutineContext(Dispatchers.IO)
        .build()
}


