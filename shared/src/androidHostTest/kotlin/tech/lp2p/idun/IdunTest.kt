package tech.lp2p.idun

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import tech.lp2p.idun.core.SHA1_HASH_LENGTH
import tech.lp2p.idun.core.createRandomKey
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


class IdunTest {

    @Test
    fun storeKeyAndCoverage(): Unit = runBlocking(Dispatchers.IO) {
        val idun = newIdun()

        val id = idun.nodeId

        val random = createRandomKey(SHA1_HASH_LENGTH)
        idun.storeKey(random)

        val samples = idun.entries(random, 10)
        assertFalse(samples.isEmpty())
        assertTrue(samples.size == 1)

        val client = Idun()
        client.ping(idun.address(), id)

        idun.shutdown()
    }

    @Test
    fun bootstrapTest() {
        var address = bootstrap()
        assertNotNull(address)
        assertTrue(address.isNotEmpty())
    }

}