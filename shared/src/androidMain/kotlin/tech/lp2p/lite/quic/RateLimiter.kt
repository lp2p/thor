package tech.lp2p.lite.quic

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.updateAndGet

internal interface Limiter {
    suspend fun run()
}

internal class RateLimiter {
    private val nextOccasion = atomic(1)
    private val attempts = atomic(0)


    suspend fun execute(runnable: Limiter) {
        if (attempts.incrementAndGet() == nextOccasion.value) {
            runnable.run()
            nextOccasion.updateAndGet { i: Int -> i * Settings.FACTOR }
        }
    }

}
