package tech.lp2p.lite.quic


class Protocols : HashMap<String, Handler>() {
    fun names(): Set<String> {
        return keys.toSet()
    }
}