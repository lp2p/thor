package tech.lp2p.lite.protos

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import tech.lp2p.asen.PeerId
import tech.lp2p.lite.quic.Handler
import tech.lp2p.lite.quic.Stream

class IdentifyHandler(val peerId: PeerId) : Handler {


    @OptIn(ExperimentalSerializationApi::class)
    override suspend fun protocol(stream: Stream) {

        val identify: Identify = identify(
            peerId, "lite/1.0.0/", stream.connection.responder().protocols.names()
        )
        stream.writeOutput(
            true,
            encode(ProtoBuf.encodeToByteArray(identify), MULTISTREAM_PROTOCOL, IDENTITY_PROTOCOL)
        )
    }


    override suspend fun data(stream: Stream, data: ByteArray) {
        if (data.isNotEmpty()) {
            throw Exception("not expected data received for identify")
        }
    }
}
