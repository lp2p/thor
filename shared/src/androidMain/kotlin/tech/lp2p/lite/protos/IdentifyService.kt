package tech.lp2p.lite.protos

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.identifyPeerId
import tech.lp2p.lite.TIMEOUT
import tech.lp2p.lite.quic.Connection
import tech.lp2p.lite.quic.Requester


internal fun identify(
    self: PeerId,
    agent: String,
    protocols: Set<String>
): Identify {
    return Identify(
        identifyPeerId(self), emptyList(), protocols.toList(), null, PROTOCOL_VERSION, agent
    )
}


@OptIn(ExperimentalSerializationApi::class)
internal suspend fun identify(connection: Connection): Identify {
    val data = Requester.createStream(connection)
        .request(
            TIMEOUT.toLong(), encode(
                MULTISTREAM_PROTOCOL,
                IDENTITY_PROTOCOL
            )
        )
    val response = receiveResponse(data)

    return ProtoBuf.decodeFromByteArray<Identify>(response)
}


private const val PROTOCOL_VERSION: String = "ipfs/0.1.0"

