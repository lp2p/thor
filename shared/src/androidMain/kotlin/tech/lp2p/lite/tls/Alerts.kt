package tech.lp2p.lite.tls

class BadCertificateAlert(message: String?) :
    ErrorAlert(message, AlertDescription.BAD_CERTIFICATE)

class BadRecordMacAlert(message: String?) :
    ErrorAlert(message, AlertDescription.BAD_RECORD_MAC)

class DecryptErrorAlert(message: String?) :
    ErrorAlert(message, AlertDescription.DECRYPT_ERROR)

class InternalErrorAlert(message: String?) :
    ErrorAlert(message, AlertDescription.INTERNAL_ERROR)

class HandshakeFailureAlert(message: String?) :
    ErrorAlert(message, AlertDescription.HANDSHAKE_FAILURE)

class IllegalParameterAlert(message: String?) :
    ErrorAlert(message, AlertDescription.ILLEGAL_PARAMETER)


class MissingExtensionAlert : ErrorAlert {
    constructor() : super("missing extension", AlertDescription.MISSING_EXTENSION)

    constructor(message: String?) : super(message, AlertDescription.MISSING_EXTENSION)
}

class UnexpectedMessageAlert(message: String?) :
    ErrorAlert(message, AlertDescription.UNEXPECTED_MESSAGE)

class UnsupportedExtensionAlert(message: String?) :
    ErrorAlert(message, AlertDescription.UNSUPPORTED_EXTENSION)