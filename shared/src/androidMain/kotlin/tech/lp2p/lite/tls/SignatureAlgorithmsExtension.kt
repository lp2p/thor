/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * The TLS supported groups extension.
 * See [...](https://tools.ietf.org/html/rfc8446#section-4.2.3)
 * "Note: This enum is named "SignatureScheme" because there is already a "SignatureAlgorithm" payloadType in TLS 1.2,
 * which this replaces.  We use the term "signature algorithm" throughout the text."
 */
data class SignatureAlgorithmsExtension(val algorithms: List<SignatureScheme>) : Extension {
    override fun getBytes(): ByteArray {
        val extensionLength = 2 + algorithms.size * 2
        val buffer = Buffer()
        buffer.writeShort(ExtensionType.SIGNATURE_ALGORITHMS.value)
        buffer.writeShort(extensionLength.toShort()) // Extension data length (in bytes)

        buffer.writeShort((algorithms.size * 2).toShort())
        for (namedGroup in algorithms) {
            buffer.writeShort(namedGroup.value)
        }
        require(buffer.size.toInt() == 4 + extensionLength)
        return buffer.readByteArray()
    }


    companion object {
        fun parse(buffer: Buffer, extensionLength: Int): SignatureAlgorithmsExtension {
            val algorithms: MutableList<SignatureScheme> = ArrayList()
            val extensionDataLength = validateExtensionHeader(
                buffer, extensionLength, 2 + 2
            )
            val supportedAlgorithmsLength = buffer.readShort().toInt()
            if (extensionDataLength != 2 + supportedAlgorithmsLength) {
                throw DecodeErrorException("inconsistent length")
            }
            if (supportedAlgorithmsLength % 2 != 0) {
                throw DecodeErrorException("invalid group length")
            }

            var i = 0
            while (i < supportedAlgorithmsLength) {
                val supportedAlgorithmsBytes = (buffer.readShort() % 0xffff).toShort()
                val algorithm: SignatureScheme =
                    SignatureScheme.get(supportedAlgorithmsBytes)
                algorithms.add(algorithm)
                i += 2
            }

            return SignatureAlgorithmsExtension(algorithms)
        }
    }
}
