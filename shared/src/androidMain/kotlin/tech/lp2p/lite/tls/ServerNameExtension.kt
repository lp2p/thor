/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * TLS server name extension: RFC 6066
 * [...](https://tools.ietf.org/html/rfc6066#section-3)
 */
data class ServerNameExtension(val serverName: String?) : Extension {
    override fun getBytes(): ByteArray {
        val hostnameLength = serverName!!.length.toShort()
        val extensionLength = (hostnameLength + 2 + 1 + 2).toShort()

        val buffer = Buffer()

        buffer.writeShort(ExtensionType.SERVER_NAME.value)
        buffer.writeShort(extensionLength) // Extension data length (in bytes)

        // https://tools.ietf.org/html/rfc6066#section-3
        buffer.writeShort((hostnameLength + 1 + 2).toShort()) // Length of server_name_list
        buffer.writeByte(0x00.toByte()) // list entry is payloadType 0x00 "DNS hostname"
        buffer.writeShort(hostnameLength) // Length of hostname
        buffer.write(serverName.toByteArray(Charsets.US_ASCII))
        require(buffer.size.toInt() == 4 + extensionLength)
        return buffer.readByteArray()
    }

    companion object {
        fun parse(buffer: Buffer, extensionLength: Int): ServerNameExtension {
            val serverName: String?
            val extensionDataLength =
                validateExtensionHeader(buffer, extensionLength, 0)
            if (extensionDataLength > 0) {
                if (extensionDataLength < 2) {
                    throw DecodeErrorException("incorrect extension length")
                }
                val serverNameListLength = buffer.readShort().toInt()
                if (extensionDataLength != serverNameListLength + 2) {
                    throw DecodeErrorException("inconsistent length")
                }
                serverName = parseServerName(buffer)
            } else {
                // https://tools.ietf.org/html/rfc6066#section-3
                // " A server that receives a client hello containing the "server_name" extension (...). In this event,
                // the server SHALL include an extension of payloadType "server_name" in the (extended) server hello.
                // The "extension_data" field of this extension SHALL be empty."
                serverName = null
            }
            return ServerNameExtension(serverName)
        }


        private fun parseServerName(buffer: Buffer): String {
            val nameType = buffer.readByte().toInt()
            if (nameType == 0) { // host_name
                val hostNameLength = buffer.readShort().toInt() and 0xffff
                if (hostNameLength > buffer.size) {
                    throw DecodeErrorException("extension underflow")
                }
                val hostNameBytes = buffer.readByteArray(hostNameLength)
                // "The hostname is represented as a byte string using ASCII encoding without a trailing dot. "
                return String(hostNameBytes, Charsets.US_ASCII)
            }
            // unsupported payloadType, RFC 6066 only defines hostname
            throw DecodeErrorException("invalid NameType")
        }
    }
}
