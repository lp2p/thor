package tech.lp2p.idun.core

import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import tech.lp2p.idun.debug
import tech.lp2p.idun.isDebug
import tech.lp2p.idun.messages.Error
import tech.lp2p.idun.messages.Message
import tech.lp2p.idun.messages.Request
import tech.lp2p.idun.messages.Response
import java.util.concurrent.ThreadLocalRandom

class Call(val request: Request, val expectedID: ByteArray?) {
    private val listeners: MutableList<CallListener> = ArrayList(3)
    private val mutex = Mutex()
    var sentTime: Long = -1
        private set
    private var responseTime: Long = -1

    var expectedRTT: Long = -1
    private var state = CallState.UNSENT
    var response: Message? = null
        private set
    private var sourceWasKnownReachable = false
    private var socketMismatch = false
    private var timeoutTimer: Job? = null


    fun builtFromEntry(e: BucketEntry) {
        sourceWasKnownReachable = e.verifiedReachable()
    }

    fun knownReachableAtCreationTime(): Boolean {
        return sourceWasKnownReachable
    }


    fun matchesExpectedID(): Boolean {
        return expectedID!! contentEquals (response!!.id)
    }

    fun setSocketMismatch() {
        socketMismatch = true
    }

    fun hasSocketMismatch(): Boolean {
        return socketMismatch
    }

    /**
     * when external circumstances indicate that this request is probably stalled and will time out
     */
    suspend fun injectStall() {
        stateTransition(setOf(CallState.SENT), CallState.STALLED)
    }

    suspend fun response(rsp: Message) {
        timeoutTimer?.cancel()

        response = rsp

        when (rsp) {
            is Response -> stateTransition(
                setOf(CallState.SENT, CallState.STALLED),
                CallState.RESPONDED
            )

            is Error -> {
                debug("RPCCall", "received non-response [$rsp] in response to request: $request")
                stateTransition(setOf(CallState.SENT, CallState.STALLED), CallState.ERROR)
            }

            else -> throw IllegalStateException("should not happen")
        }
    }


    fun addListener(cl: CallListener) {
        check(state == CallState.UNSENT) { "can only attach listeners while call is not started yet" }
        listeners.add(cl)
    }


    suspend fun hasSend(node: Node) {
        if (isDebug && expectedRTT <= 0) {
            throw AssertionError("Assertion failed")
        }
        if (isDebug && expectedRTT > RPC_CALL_TIMEOUT_MAX) {
            throw AssertionError("Assertion failed")
        }
        sentTime = System.currentTimeMillis()


        stateTransition(setOf(CallState.UNSENT), CallState.SENT)


        // spread out the stalls by +- 1ms to reduce lock contention
        val smear = ThreadLocalRandom.current().nextInt(-1000, 1000)
        timeoutTimer = node.scope.launch {
            delay(expectedRTT * 1000 + smear)
            checkStallOrTimeout(node)
        }
    }


    private suspend fun checkStallOrTimeout(node: Node) {
        mutex.withLock {
            if (state != CallState.SENT && state != CallState.STALLED) return
            val elapsed = System.currentTimeMillis() - sentTime
            val remaining = RPC_CALL_TIMEOUT_MAX - elapsed
            if (remaining > 0) {
                stateTransition(setOf(CallState.SENT), CallState.STALLED)
                // re-schedule for failed
                timeoutTimer = node.scope.launch {
                    delay(remaining)
                    checkStallOrTimeout(node)
                }
            } else {
                stateTransition(setOf(CallState.SENT, CallState.STALLED), CallState.TIMEOUT)
            }
        }
    }

    suspend fun sendFailed() {
        stateTransition(setOf(CallState.UNSENT), CallState.TIMEOUT)
    }

    suspend fun cancel() {
        val timer = timeoutTimer
        timer?.cancel()
        // it would be better if we didn't have to treat this as a timeout and could
        // just signal call termination with an internal reason
        stateTransition(
            setOf(
                CallState.UNSENT,
                CallState.SENT,
                CallState.STALLED
            ), CallState.TIMEOUT
        )
    }


    private suspend fun stateTransition(expected: Set<CallState>, newState: CallState) {
        mutex.withLock {
            val oldState = state
            if (!expected.contains(oldState)) {
                return
            }

            state = newState


            when (newState) {
                CallState.ERROR, CallState.RESPONDED -> responseTime = System.currentTimeMillis()
                else -> {}
            }
            for (i in listeners.indices) {
                val l = listeners[i]
                l.stateTransition(this, oldState, newState)

                when (newState) {
                    CallState.TIMEOUT -> l.onTimeout(this)
                    CallState.STALLED -> {}
                    CallState.RESPONDED -> l.onResponse(this, response!!)
                    CallState.UNSENT -> {}
                    CallState.SENT -> {}
                    CallState.ERROR -> {}
                }
            }
        }
    }

    val rTT: Long
        get() {
            if (sentTime == -1L || responseTime == -1L) return -1
            return responseTime - sentTime
        }

    fun state(): CallState {
        return state
    }

}
