package tech.lp2p.idun.core

import kotlinx.atomicfu.atomic
import tech.lp2p.idun.messages.Message
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

class ResponseTimeoutFilter {
    private val bins = FloatArray(NUM_BINS)
    private val updateCount = atomic(0L)
    var currentStats: Snapshot = Snapshot(
        tapInvoke(
            bins.clone()
        ) { ary: FloatArray -> ary[ary.size - 1] = 1.0f })

    private var timeoutCeiling: Long = 0
    private var timeoutBaseline: Long = 0
    private val listener: CallListener = object : CallListener {
        override suspend fun stateTransition(call: Call, previous: CallState, current: CallState) {
        }

        override fun onResponse(call: Call, rsp: Message) {
            updateAndRecalc(call.rTT)
        }

        override suspend fun onTimeout(call: Call) {
        }
    }

    init {
        reset()
    }

    fun reset() {
        updateCount.value = 0
        timeoutCeiling = RPC_CALL_TIMEOUT_MAX.toLong()
        timeoutBaseline = timeoutCeiling
        bins.fill(1.0f / bins.size)
    }


    fun registerCall(call: Call) {
        call.addListener(listener)
    }

    private fun updateAndRecalc(newRTT: Long) {
        update(newRTT)
        if ((updateCount.incrementAndGet() and 0x0fL) == 0L) {
            newSnapshot()
            decay()
        }
    }

    private fun update(newRTT: Long) {
        var bin = (newRTT - MIN_BIN).toInt() / BIN_SIZE
        bin = max(min(bin.toDouble(), (bins.size - 1).toDouble()), 0.0).toInt()

        bins[bin] += 1.0f
    }

    private fun decay() {
        for (i in bins.indices) {
            bins[i] *= 0.95f
        }
    }


    private fun newSnapshot() {
        currentStats = Snapshot(bins.clone())
        timeoutBaseline = currentStats.getQuantile(0.1f).toLong()
        timeoutCeiling = currentStats.getQuantile(0.9f).toLong()
    }

    val stallTimeout: Long
        get() =// either the 90th percentile or the 10th percentile + 100ms baseline, whichever is HIGHER (to prevent descent to zero and missing more than 10% of the packets in the worst case).
            // but At most RPC_CALL_TIMEOUT_MAX
            min(
                max(
                    (timeoutBaseline + RPC_CALL_TIMEOUT_BASELINE_MIN).toDouble(),
                    timeoutCeiling.toDouble()
                ), RPC_CALL_TIMEOUT_MAX.toDouble()
            ).toLong()

    class Snapshot internal constructor(val values: FloatArray) {
        private var mean: Float = 0f


        init {
            normalize()

            calcStats()
        }

        private fun normalize() {
            var cumulativePopulation = 0f

            for (value in values) {
                cumulativePopulation += value
            }

            if (cumulativePopulation > 0) for (i in values.indices) {
                values[i] /= cumulativePopulation
            }
        }

        private fun calcStats() {
            var modePop = 0f

            for (bin in values.indices) {
                mean += values[bin] * (bin + 0.5f) * BIN_SIZE
                if (values[bin] > modePop) {
                    modePop = values[bin]
                }
            }
        }

        fun getQuantile(value: Float): Float {
            var quant = value
            for (i in values.indices) {
                quant -= values[i]
                if (quant <= 0) return (i + 0.5f) * BIN_SIZE
            }

            return MAX_BIN.toFloat()
        }
    }

}

fun tapInvoke(obj: FloatArray, c: (FloatArray) -> Unit): FloatArray {
    c.invoke(obj)
    return obj
}

private const val MIN_BIN = 0
private const val MAX_BIN = RPC_CALL_TIMEOUT_MAX
private const val BIN_SIZE = 50
private val NUM_BINS = ceil(((MAX_BIN - MIN_BIN) * 1.0f / BIN_SIZE).toDouble()).toInt()