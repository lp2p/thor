package tech.lp2p.idun.core

data class Address(val address: ByteArray, val port: UShort) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Address

        if (!address.contentEquals(other.address)) return false
        if (port != other.port) return false

        return true
    }


    private fun numericInet4(): String {
        return (address[0].toInt() and 255).toString() + "." +
                (address[1].toInt() and 255) + "." +
                (address[2].toInt() and 255) + "." + (address[3].toInt() and 255)
    }

    fun inet4(): Boolean {
        return address.size == 4
    }

    private fun numericInet6(): String {
        val builder = StringBuilder(39)

        for (var2 in 0..7) {
            builder.append(
                Integer.toHexString(
                    address[var2 shl 1].toInt() shl 8 and '\uff00'.code or
                            (address[(var2 shl 1) + 1].toInt() and 255)
                )
            )
            if (var2 < 7) {
                builder.append(":")
            }
        }

        return builder.toString()
    }

    fun address(): String {
        if (inet4()) {
            return numericInet4()
        }
        return numericInet6()
    }

    override fun hashCode(): Int {
        var result = address.contentHashCode()
        result = 31 * result + port.hashCode()
        return result
    }
}