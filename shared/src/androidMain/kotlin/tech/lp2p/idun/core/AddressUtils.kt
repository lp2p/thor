package tech.lp2p.idun.core

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import kotlinx.io.readUShort
import kotlinx.io.writeUShort
import java.net.Inet6Address
import java.net.InetAddress
import java.net.NetworkInterface


fun packAddress(addr: Address): ByteArray {
    val buffer = Buffer()
    buffer.write(addr.address)
    buffer.writeUShort(addr.port)
    return buffer.readByteArray()
}


internal fun writeBuckets(list: List<BucketEntry>, length: Int): StringWriter {
    return object : StringWriter {
        override fun writeTo(buf: Buffer) {
            list.forEach { e: BucketEntry ->
                val sockAddr = e.address
                buf.write(e.id)
                buf.write(sockAddr.address)
                buf.writeUShort(sockAddr.port.toUShort())
            }
        }

        override fun length(): Int {
            return list.size * length
        }
    }
}

internal fun readBuckets(src: ByteArray, length: Int): List<BucketEntry> {
    val buffer = Buffer()
    buffer.write(src)

    val result = mutableListOf<BucketEntry>()
    while (!buffer.exhausted()) {
        val rawId = buffer.readByteArray(SHA1_HASH_LENGTH)
        val raw = buffer.readByteArray(length - 2) // -2 because of port
        val port = buffer.readUShort()

        result.add(
            BucketEntry(
                Address(raw, port), rawId // must be cloned
            )
        )
    }
    return result
}


val inetAddressFromNetworkInterfaces: InetAddress
    get() {

        val networkInterfaces =
            NetworkInterface.getNetworkInterfaces()
        while (networkInterfaces.hasMoreElements()) {
            val networkInterface = networkInterfaces.nextElement()
            if (networkInterface.isUp) {
                val inetAddresses =
                    networkInterface.inetAddresses
                while (inetAddresses.hasMoreElements()) {
                    val inetAddress = inetAddresses.nextElement()
                    if (inetAddress is Inet6Address) {
                        if (!inetAddress.isLinkLocalAddress &&
                            !inetAddress.isSiteLocalAddress &&
                            !inetAddress.isMulticastAddress &&
                            !inetAddress.isAnyLocalAddress &&
                            !inetAddress.isLoopbackAddress
                        ) {
                            return inetAddress
                        }
                    }
                }
            }
        }

        // explicitly returning a loopback address here instead of null;
        // otherwise we'll depend on how JDK classes handle this,
        return InetAddress.getLoopbackAddress()
    }


fun createPeerFromAddress(address: ByteArray, port: Int): Peer {
    val buffer = Buffer()
    buffer.write(address)
    buffer.writeUShort(port.toUShort())
    return Peer(buffer.readByteArray())
}