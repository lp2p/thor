package tech.lp2p.idun.core

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.atomicArrayOfNulls
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import tech.lp2p.idun.debug
import tech.lp2p.idun.isDebug
import tech.lp2p.idun.messages.Message

class Bucket internal constructor() {
    private val currentReplacementPointer = atomic(0)
    private val replacementBucket =
        atomicArrayOfNulls<BucketEntry>(MAX_ENTRIES_PER_BUCKET)

    // using arraylist here since reading/iterating is far more common than writing.
    @Volatile
    private var entries: List<BucketEntry> = emptyList()
    private val mutex = Mutex()

    init {
        // needed for bitmasking
        if (isDebug && replacementBucket.size.countOneBits() != 1) {
            throw AssertionError("Assertion failed")
        }
    }

    /**
     * Notify bucket of new incoming packet from a node, perform update or insert
     * existing nodes where appropriate
     *
     * @param newEntry The entry to insert
     */
    suspend fun insertOrRefresh(newEntry: BucketEntry) {


        val entriesRef = entries

        for (existing in entriesRef) {
            if (existing.equals(newEntry)) {
                existing.mergeInTimestamps(newEntry)
                return
            }

            if (existing.matchIPorID(newEntry)) {
                debug(
                    "KBucket",
                    "new node $newEntry claims same ID or IP as $existing," +
                            " might be impersonation attack or IP change. ignoring until " +
                            "old entry times out"
                )
                return
            }
        }

        if (newEntry.verifiedReachable()) {
            if (entriesRef.size < MAX_ENTRIES_PER_BUCKET) {
                // insert if not already in the list and we still have room
                modifyMainBucket(null, newEntry)
                return
            }

            if (replaceBadEntry(newEntry)) return

            val youngest = entriesRef[entriesRef.size - 1]

            // older entries displace younger ones (although that kind of stuff should probably
            // go through #modifyMainBucket directly)
            // entries with a 2.5times lower RTT than the current youngest one displace the
            // youngest. safety factor to prevent fibrilliation due to changing RTT-estimates /
            // to only replace when it's really worth it
            if (youngest.creationTime > newEntry.creationTime || newEntry.rTT * 2.5 < youngest.rTT) {
                modifyMainBucket(youngest, newEntry)
                // it was a useful entry, see if we can use it to replace something questionable
                insertInReplacementBucket(youngest)
                return
            }
        }

        insertInReplacementBucket(newEntry)
    }

    fun refresh(toRefresh: BucketEntry) {
        val e = entries.filter { other: BucketEntry? -> toRefresh.equals(other) }.randomOrNull()
        e?.mergeInTimestamps(toRefresh)
    }

    /**
     * mostly meant for internal use or transfering entries into a new bucket.
     * to update a bucket properly use [.insertOrRefresh]
     */
    suspend fun modifyMainBucket(toRemove: BucketEntry?, toInsert: BucketEntry?) {
        // we're synchronizing all modifications, therefore we can freely reference
        // the old entry list, it will not be modified concurrently

        mutex.withLock {
            if (toInsert != null && entries
                    .any { other: BucketEntry? -> toInsert.matchIPorID(other) }
            ) return
            val newEntries: MutableList<BucketEntry> = ArrayList(entries)
            var removed = false
            var added = false

            // removal never violates ordering constraint, no checks required
            if (toRemove != null) removed = newEntries.remove(toRemove)


            if (toInsert != null) {
                val oldSize = newEntries.size
                val wasFull = oldSize >= MAX_ENTRIES_PER_BUCKET
                val youngest = if (oldSize > 0) newEntries[oldSize - 1] else null
                val unorderedInsert =
                    youngest != null && toInsert.creationTime < youngest.creationTime
                added = !wasFull || unorderedInsert
                if (added) {
                    newEntries.add(toInsert)
                    val entry = removeFromReplacement(toInsert)
                    entry?.mergeInTimestamps(toInsert)
                } else {
                    insertInReplacementBucket(toInsert)
                }

                if (unorderedInsert) newEntries.sortWith(AGE_ORDER)

                if (wasFull && added) while (newEntries.size > MAX_ENTRIES_PER_BUCKET) insertInReplacementBucket(
                    newEntries.removeAt(newEntries.size - 1)
                )
            }

            // make changes visible
            if (added || removed) entries = newEntries
        }
    }

    val numEntries: Int
        /**
         * Get the number of entries.
         *
         * @return The number of entries in this Bucket
         */
        get() = entries.size

    val isFull: Boolean
        get() = entries.size >= MAX_ENTRIES_PER_BUCKET

    fun getEntries(): List<BucketEntry> {
        return ArrayList(entries)
    }

    fun entries(): List<BucketEntry> {
        return entries
    }


    val replacementEntries: List<BucketEntry>
        get() {
            val repEntries: MutableList<BucketEntry> =
                ArrayList(replacementBucket.size)
            val current = currentReplacementPointer.value
            for (i in 1..replacementBucket.size) {
                val e =
                    replacementBucket[(current + i) % replacementBucket.size].value
                if (e != null) repEntries.add(e)
            }
            return repEntries
        }

    /**
     * A peer failed to respond
     *
     * @param addr Address of the peer
     */
    suspend fun onTimeout(addr: Address) {
        val entriesRef = entries
        run {
            var i = 0
            val n = entriesRef.size
            while (i < n) {
                val e = entriesRef[i]
                if (e.address == addr) {
                    e.signalRequestTimeout()
                    //only removes the entry if it is bad
                    removeEntryIfBad(e, false)
                    return
                }
                i++
            }
        }

        var i = 0
        val n = replacementBucket.size
        while (i < n) {
            val e = replacementBucket[i].value
            if (e != null && e.address == addr) {
                e.signalRequestTimeout()
                return
            }
            i++
        }
    }


    override fun toString(): String {
        return "entries: $entries replacements: $replacementBucket"
    }

    /**
     * Tries to instert entry by replacing a bad entry.
     *
     * @param entry Entry to insert
     * @return true if replace was successful
     */
    private suspend fun replaceBadEntry(entry: BucketEntry): Boolean {
        val entriesRef = entries
        var i = 0
        val n = entriesRef.size
        while (i < n) {
            val e = entriesRef[i]
            if (e.needsReplacement()) {
                // bad one get rid of it
                modifyMainBucket(e, entry)
                return true
            }
            i++
        }
        return false
    }

    private fun pollVerifiedReplacementEntry(): BucketEntry? {
        while (true) {
            var bestIndex = -1
            var bestFound: BucketEntry? = null

            for (i in 0 until replacementBucket.size) {
                val entry = replacementBucket[i].value
                if (entry == null || !entry.verifiedReachable()) continue
                val isBetter =
                    bestFound == null || entry.rTT < bestFound.rTT ||
                            (entry.rTT == bestFound.rTT && entry.lastSeen > bestFound.lastSeen)

                if (isBetter) {
                    bestFound = entry
                    bestIndex = i
                }
            }

            if (bestFound == null) return null

            var newPointer = bestIndex - 1
            if (newPointer < 0) newPointer = replacementBucket.size - 1
            if (replacementBucket[bestIndex].compareAndSet(bestFound, null)) {
                currentReplacementPointer.value = newPointer
                return bestFound
            }
        }
    }

    private fun removeFromReplacement(toRemove: BucketEntry): BucketEntry? {
        for (i in 0 until replacementBucket.size) {
            val e = replacementBucket[i].value
            if (e == null || !e.matchIPorID(toRemove)) continue
            replacementBucket[i].compareAndSet(e, null)
            if (e.equals(toRemove)) return e
        }
        return null
    }

    private fun insertInReplacementBucket(toInsert: BucketEntry?) {
        if (toInsert == null) return

        outer@ while (true) {
            val insertationPoint =
                currentReplacementPointer.incrementAndGet() and (replacementBucket.size - 1)

            val toOverwrite = replacementBucket[insertationPoint].value

            var canOverwrite: Boolean

            if (toOverwrite == null) {
                canOverwrite = true
            } else {
                val lingerTime =
                    if (toOverwrite.verifiedReachable() && !toInsert.verifiedReachable()) 5 * 60 * 1000 else 1000
                canOverwrite =
                    toInsert.lastSeen - toOverwrite.lastSeen > lingerTime || toInsert.rTT < toOverwrite.rTT
            }

            if (!canOverwrite) break

            for (i in 0 until replacementBucket.size) {
                // don't insert if already present
                val potentialDuplicate = replacementBucket[i].value
                if (toInsert.matchIPorID(potentialDuplicate)) {
                    if (toInsert.equals(potentialDuplicate)) potentialDuplicate!!.mergeInTimestamps(
                        toInsert
                    )
                    break@outer
                }
            }

            if (replacementBucket[insertationPoint].compareAndSet(toOverwrite, toInsert)) break
        }
    }

    fun findByIPorID(ip: Address?, id: ByteArray?): BucketEntry? {
        return entries.firstOrNull { e: BucketEntry -> e.id.contentEquals(id) || e.address == ip }
    }

    fun notifyOfResponse(msg: Message, associatedCall: Call) {
        val entriesRef = entries
        var i = 0
        val n = entriesRef.size
        while (i < n) {
            val entry = entriesRef[i]

            // update last responded. insert will be invoked soon,
            // thus we don't have to do the move-to-end stuff
            if (entry.id.contentEquals(msg.id)) {
                entry.signalResponse(associatedCall.rTT)
                return
            }
            i++
        }
    }


    /**
     * @param toRemove Entry to remove, if its bad
     * @param force    if true entry will be removed regardless of its state
     */
    suspend fun removeEntryIfBad(toRemove: BucketEntry, force: Boolean) {
        val entriesRef = entries
        if (entriesRef.contains(toRemove) && (force || toRemove.needsReplacement())) {
            val replacement = pollVerifiedReplacementEntry()

            // only remove if we have a replacement or really need to
            if (replacement != null || force) modifyMainBucket(toRemove, replacement)
        }
    }
}
