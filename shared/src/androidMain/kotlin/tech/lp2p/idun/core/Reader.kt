package tech.lp2p.idun.core


class Reader(val data: ByteArray, val size: Int) {
    private var pos: Int = 0

    fun remaining(): Int {
        return size - pos
    }

    fun getByteArray(length: Int): ByteArray {
        val end: Int = pos + length
        val result = data.copyOfRange(pos, end)
        pos = end
        return result
    }

    fun limit(): Int {
        return size
    }

    fun position(): Int {
        return pos
    }

    fun position(pos: Int) {
        this.pos = pos
    }

    fun getByte(): Byte {
        val byte = data[pos]
        pos++
        return byte
    }

}

fun newReader(bytes: ByteArray): Reader {
    return Reader(bytes, bytes.size)
}

