package tech.lp2p.idun.tasks

import tech.lp2p.idun.core.Node


abstract class IteratingTask internal constructor(key: ByteArray, node: Node) :
    Task(key, node) {
    val closest: ClosestSet = ClosestSet(key)

    val todo: IterativeLookupCandidates =
        IterativeLookupCandidates(
            key, node.mismatchDetector,
            node.unreachableCache
        )

    override fun getTodoCount(): Int {
        return todo.allCand().count(todo.lookupFilter).toInt()
    }
}
