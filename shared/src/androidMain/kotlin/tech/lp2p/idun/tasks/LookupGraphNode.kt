package tech.lp2p.idun.tasks

import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.Call
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.CopyOnWriteArraySet
import kotlin.math.ceil
import kotlin.math.max

class LookupGraphNode internal constructor(val bucketEntry: BucketEntry) {

    val sources: MutableSet<LookupGraphNode> = CopyOnWriteArraySet()
    private val backingStore: MutableSet<LookupGraphNode> = ConcurrentHashMap.newKeySet()
    val calls: MutableList<Call> = CopyOnWriteArrayList()
    var tainted: Boolean = false
    private var acceptedResponse: Boolean = false
    var root: Boolean = false
    var unreachable: Boolean = false
    var throttled: Boolean = false

    fun addCall(c: Call) {
        calls.add(c)
    }

    fun addSource(toAdd: LookupGraphNode) {
        sources.add(toAdd)
    }

    private fun callsNotSuccessful(): Boolean {
        return calls.isNotEmpty() && !wasAccepted()
    }

    fun nonSuccessfulDescendantCalls(): Int {
        return ceil(
            if (backingStore.isEmpty()) 0.0 else backingStore
                .filter { obj: LookupGraphNode -> obj.callsNotSuccessful() }
                .sumOf { node: LookupGraphNode ->
                    1.0 / max(
                        node.sources.size.toDouble(),
                        1.0
                    )
                }
        ).toInt()
    }

    fun addChildren(toAdd: Collection<LookupGraphNode>) {
        for (e in toAdd) {
            if (backingStore.contains(e)) return
            backingStore.add(e)
        }
    }

    fun toKbe(): BucketEntry {
        return bucketEntry
    }

    fun accept() {
        acceptedResponse = true
    }

    private fun wasAccepted(): Boolean {
        return acceptedResponse
    }

    override fun equals(other: Any?): Boolean {
        if (other is LookupGraphNode) {
            return bucketEntry.equals(other.bucketEntry)
        }
        return false
    }

    override fun hashCode(): Int {
        return bucketEntry.hashCode()
    }


    override fun toString(): String {
        return "LookupNode desc:" + nonSuccessfulDescendantCalls()
    }
}
