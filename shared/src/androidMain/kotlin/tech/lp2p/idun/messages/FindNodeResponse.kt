package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.NODE_ENTRY_LENGTH_IPV4
import tech.lp2p.idun.core.NODE_ENTRY_LENGTH_IPV6
import tech.lp2p.idun.core.encodeInto
import tech.lp2p.idun.core.packAddress
import tech.lp2p.idun.core.writeBuckets

data class FindNodeResponse(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray,
    val nodes: List<BucketEntry>,
    val nodes6: List<BucketEntry>
) : Response {

    override fun encode(buffer: Buffer) {
        val base: MutableMap<String, Any> = mutableMapOf()
        val inner: MutableMap<String, Any> = mutableMapOf()
        inner["id"] = id
        if (nodes.isNotEmpty()) inner["nodes"] = writeBuckets(nodes, NODE_ENTRY_LENGTH_IPV4)
        if (nodes6.isNotEmpty()) inner["nodes6"] = writeBuckets(nodes6, NODE_ENTRY_LENGTH_IPV6)
        base["r"] = inner

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "r"

        // message method if we are a response
        base["ip"] = packAddress(address)

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FindNodeResponse

        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false
        if (nodes != other.nodes) return false
        if (nodes6 != other.nodes6) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        result = 31 * result + nodes.hashCode()
        result = 31 * result + nodes6.hashCode()
        return result
    }

}
