package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.encodeInto
import tech.lp2p.idun.core.packAddress

data class PingResponse(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray
) : Response {

    override fun encode(buffer: Buffer) {
        val base: MutableMap<String, Any> = mutableMapOf()
        base["r"] = mapOf<String, Any>("id" to id)

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "r"

        // message method if we are a response
        base["ip"] = packAddress(address)

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PingResponse

        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        return result
    }
}
