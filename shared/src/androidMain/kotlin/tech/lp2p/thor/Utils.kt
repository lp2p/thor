package tech.lp2p.thor

import java.util.regex.Pattern


private val CONTENT_DISPOSITION_PATTERN: Pattern = Pattern.compile(
    "attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1(\"?);",
    Pattern.CASE_INSENSITIVE
)


fun parseContentDisposition(contentDisposition: String?): String? {
    try {
        val m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition.toString())
        if (m.find()) {
            return m.group(2)
        }
    } catch (_: IllegalStateException) {
        // This function is defined as returning null when it can't parse the header
    }
    return null
}