package tech.lp2p.thor

import kotlinx.coroutines.runBlocking
import kotlinx.io.Buffer
import tech.lp2p.dark.core.EOF
import tech.lp2p.halo.Channel
import java.io.IOException
import java.io.InputStream
import kotlin.math.min

class Stream(private val channel: Channel) : InputStream() {
    private var buffer: Buffer? = null

    override fun available(): Int {
        return channel.size().toInt()
    }

    // todo maybe no blocking here
    private fun loadNextData(): Unit = runBlocking {
        try {
            buffer = channel.next()
        } catch (throwable: Throwable) {
            throw IOException(throwable)
        }
    }

    private fun hasData(): Boolean {
        if (buffer == null) { // initial the buffer is null
            loadNextData()
        }
        if (buffer == null) {
            return false
        }
        if (buffer!!.exhausted()) {
            return false
        }
        return true
    }

    override fun read(bytes: ByteArray, off: Int, len: Int): Int {
        if (!hasData()) return EOF

        val max = min(len, buffer!!.size.toInt())
        val read = buffer!!.readAtMostTo(bytes, off, off + max)

        if (buffer!!.exhausted()) {
            loadNextData()
        }

        return read
    }

    override fun read(bytes: ByteArray): Int {
        return read(bytes, 0, bytes.size)
    }


    override fun read(): Int {
        if (!hasData()) return EOF
        if (!buffer!!.exhausted()) {
            return buffer!!.readByte().toInt() and 0xFF
        } else {
            loadNextData()
            return read()
        }
    }


    override fun skip(n: Long): Long {
        channel.seek(n)
        loadNextData()
        return n
    }
}