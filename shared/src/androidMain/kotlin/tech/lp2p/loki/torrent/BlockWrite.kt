package tech.lp2p.loki.torrent


data class BlockWrite(
    val error: Throwable?,
    val isRejected: Boolean,
    val verified: Boolean
)

fun rejectBlockWrite(): BlockWrite {
    return BlockWrite(null, true, false)
}

fun completeBlockWrite(verified: Boolean): BlockWrite {
    return BlockWrite(null, false, verified)
}

fun exceptionalBlockWrite(error: Throwable?): BlockWrite {
    return BlockWrite(error, false, false)
}