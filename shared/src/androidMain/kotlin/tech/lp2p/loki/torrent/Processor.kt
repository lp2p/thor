package tech.lp2p.loki.torrent

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import tech.lp2p.loki.Loki
import tech.lp2p.loki.Progress
import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.data.DataDescriptor
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.ExtendedHandshakeConsumer
import tech.lp2p.loki.net.InetPeerAddress
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.net.createPeer
import tech.lp2p.loki.protocol.BitOrder
import java.util.concurrent.ConcurrentHashMap
import java.util.stream.IntStream


internal suspend fun process(
    loki: Loki, magnetUri: MagnetUri,
    period: Long,
    progress: Progress
) {
    processFetchMetadataStage(loki, magnetUri)
    processInitializeTorrent(loki)
    processInitializeMagnetTorrent(loki)
    processChooseFiles(loki)
    processProcessTorrent(loki, period, progress)
}


private suspend fun processProcessTorrent(
    loki: Loki, period: Long,
    progress: Progress
): Unit = coroutineScope {
    val descriptor = loki.descriptor

    descriptor.start()

    loki.idun.storeKey(loki.torrentId.bytes)

    while (isActive) {
        val state = loki.torrentState
        if (state.piecesRemaining == 0) {
            loki.storage.finish()
            break
        } else {
            delay(period)
            progress.progress(state.piecesComplete, state.piecesTotal)
        }
    }
}

private fun processChooseFiles(loki: Loki) {
    val torrent = loki.torrent
    checkNotNull(torrent)
    val descriptor = loki.descriptor
    checkNotNull(descriptor)

    val selectedFiles: Set<TorrentFile> = torrent.files.toSet()
    val bitfield = descriptor.dataDescriptor!!.bitfield
    val validPieces = getValidPieces(descriptor.dataDescriptor!!, selectedFiles)
    val selector = createSelector(RarestFirstSelector(), bitfield, validPieces)
    val pieceStatistics = loki.pieceStatistics
    val assignments = Assignments(
        bitfield, selector, pieceStatistics!!
    )

    updateSkippedPieces(bitfield, validPieces)
    loki.assignments = assignments
}

private fun updateSkippedPieces(bitfield: Bitfield, validPieces: Set<Int>) {
    IntStream.range(0, bitfield.piecesTotal).forEach { pieceIndex: Int ->
        if (!validPieces.contains(pieceIndex)) {
            bitfield.skip(pieceIndex)
        }
    }
}

private fun getValidPieces(
    dataDescriptor: DataDescriptor,
    selectedFiles: Set<TorrentFile>
): Set<Int> {
    val validPieces: MutableSet<Int> = HashSet()
    IntStream.range(0, dataDescriptor.bitfield.piecesTotal).forEach { pieceIndex: Int ->
        for (file in dataDescriptor.getFilesForPiece(pieceIndex)) {
            if (selectedFiles.contains(file)) {
                validPieces.add(pieceIndex)
                break
            }
        }
    }
    return validPieces
}

private fun createSelector(
    selector: RarestFirstSelector,
    bitfield: Bitfield,
    selectedFilesPieces: Set<Int>
): ValidatingSelector {
    val validator = { pieceIndex: Int ->
        selectedFilesPieces.contains(pieceIndex) &&
                !isPieceComplete(bitfield, pieceIndex)
    }
    return ValidatingSelector(selector, validator)
}

private fun isPieceComplete(bitfield: Bitfield, pieceIndex: Int): Boolean {
    val pieceStatus = bitfield.getPieceStatus(pieceIndex)
    return pieceStatus == Bitfield.PieceStatus.COMPLETE ||
            pieceStatus == Bitfield.PieceStatus.COMPLETE_VERIFIED
}

private fun processFetchMetadataStage(loki: Loki, magnetUri: MagnetUri) {
    val torrentId = loki.torrentId

    val metadataConsumer = MetadataConsumer(loki, torrentId)
    loki.messageRouter.registerMessagingAgent(metadataConsumer)

    // need to also receive Bitfields and Haves (without validation for the number of pieces...)
    val bitfieldConsumer = BitfieldCollectingConsumer()
    loki.messageRouter.registerMessagingAgent(bitfieldConsumer)

    loki.descriptor.start()

    magnetUri.peerAddresses.forEach { peerAddress: InetPeerAddress ->
        debug("FetchMetadataStage", "" + peerAddress)
        loki.addPeer(createPeer(peerAddress))
    }

    metadataConsumer.waitForTorrent()

    debug("Processor", "Torrent received")

    loki.bitfieldConsumer = bitfieldConsumer
}

private fun processInitializeTorrent(loki: Loki) {
    val torrent = loki.torrent
    checkNotNull(torrent)
    val source = loki.source
    checkNotNull(source)

    var descriptor = loki.descriptor
    val dataDescriptor = DataDescriptor(loki.storage, torrent)

    descriptor.dataDescriptor = dataDescriptor
    val bitfield = dataDescriptor.bitfield
    val pieceStatistics = createPieceStatistics(bitfield)

    loki.messageRouter.registerMessagingAgent(GenericConsumer.consumer())
    loki.messageRouter.registerMessagingAgent(
        BitfieldConsumer(bitfield, pieceStatistics)
    )
    loki.messageRouter.registerMessagingAgent(
        ExtendedHandshakeConsumer(loki)
    )
    loki.messageRouter.registerMessagingAgent(
        PieceConsumer(
            bitfield, loki.dataWorker, loki.bufferedPieceRegistry
        )
    )
    loki.messageRouter.registerMessagingAgent(
        PeerRequestConsumer(
            loki.dataWorker
        )
    )
    loki.messageRouter.registerMessagingAgent(
        RequestProducer(bitfield, dataDescriptor.chunkDescriptors)
    )
    loki.messageRouter.registerMessagingAgent(
        MetadataProducer(source, torrent.isPrivate)
    )

    loki.bitfield = bitfield
    loki.pieceStatistics = pieceStatistics
}

private fun processInitializeMagnetTorrent(loki: Loki) {
    val statistics = loki.pieceStatistics
    // process bitfields and haves that we received while fetching metadata
    val peersUpdated: MutableCollection<Peer> = HashSet()
    loki.bitfieldConsumer!!.getBitfields()
        .forEach { (peer: Peer, bitfieldBytes: ByteArray) ->
            if (statistics!!.getPeerBitfield(peer) != null) {
                // we should not have received peer's bitfields twice, but whatever.. ignore and continue
                return@forEach
            }
            peersUpdated.add(peer)
            statistics.addBitfield(
                peer,
                Bitfield(bitfieldBytes, BitOrder.LITTLE_ENDIAN, statistics.piecesTotal)
            )
        }
    loki.bitfieldConsumer!!.getHaves()
        .forEach { (peer: Peer, pieces: MutableSet<Int>) ->
            peersUpdated.add(peer)
            pieces.forEach { piece: Int -> statistics!!.addPiece(peer, piece) }
        }

    // unregistering only now, so that there were no gaps in bitfield receiving
    loki.bitfieldConsumer = null // mark for gc collection
}

private fun createPieceStatistics(bitfield: Bitfield): PieceStatistics {
    return PieceStatistics(
        bitfield, ConcurrentHashMap(), IntArray(bitfield.piecesTotal)
    )
}

