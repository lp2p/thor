package tech.lp2p.loki.torrent

import java.util.stream.IntStream

data class ValidatingSelector(
    val selector: RarestFirstSelector,
    val validator: (Int) -> Boolean,

    ) {
    fun getNextPieces(pieceStatistics: PieceStatistics): IntStream {
        return selector.getNextPieces(pieceStatistics)
            .filter(validator)
    }
}
