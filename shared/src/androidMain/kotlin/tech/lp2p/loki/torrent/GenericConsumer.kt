package tech.lp2p.loki.torrent

import tech.lp2p.loki.debug
import tech.lp2p.loki.protocol.Cancel
import tech.lp2p.loki.protocol.Choke
import tech.lp2p.loki.protocol.Interested
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.NotInterested
import tech.lp2p.loki.protocol.Type
import tech.lp2p.loki.protocol.Unchoke

class GenericConsumer : Consumers {
    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.KeepAlive
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Choke
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Unchoke
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Interested
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })

            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.NotInterested
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Cancel
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            return list
        }

    companion object {

        private val instance = GenericConsumer()

        fun consumer(): GenericConsumer {
            return instance
        }

        private fun doConsume(message: Message, messageContext: MessageContext) {
            if (message is Choke) {
                consume(message, messageContext)
            }
            if (message is Unchoke) {
                consume(message, messageContext)
            }
            if (message is Interested) {
                consume(message, messageContext)
            }
            if (message is NotInterested) {
                consume(message, messageContext)
            }
            if (message is Cancel) {
                consume(message, messageContext)
            }
        }

        private fun consume(choke: Choke, context: MessageContext) {
            debug("GenericConsumer", choke.toString())
            context.connectionState.isPeerChoking = true
        }

        private fun consume(unchoke: Unchoke, context: MessageContext) {
            debug("GenericConsumer", unchoke.toString())
            context.connectionState.isPeerChoking = false
        }

        private fun consume(interested: Interested, context: MessageContext) {
            debug("GenericConsumer", interested.toString())
            context.connectionState.isPeerInterested = true
        }

        private fun consume(notInterested: NotInterested, context: MessageContext) {
            debug("GenericConsumer", notInterested.toString())
            context.connectionState.isPeerInterested = false
        }

        private fun consume(cancel: Cancel, context: MessageContext) {
            debug("GenericConsumer", cancel.toString())
            context.connectionState.onCancel(cancel)
        }
    }
}
