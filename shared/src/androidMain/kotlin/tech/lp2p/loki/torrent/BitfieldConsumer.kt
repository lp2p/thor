package tech.lp2p.loki.torrent

import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.protocol.BitOrder
import tech.lp2p.loki.protocol.Have
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Type


class BitfieldConsumer(
    private val bitfield: Bitfield,
    private val pieceStatistics: PieceStatistics
) :
    Consumers {
    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is tech.lp2p.loki.protocol.Bitfield) {
            consume(message, messageContext)
        }
        if (message is Have) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Bitfield
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Have
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            return list
        }


    private fun consume(
        bitfieldMessage: tech.lp2p.loki.protocol.Bitfield,
        context: MessageContext
    ) {
        val peer = context.peer
        val peerBitfield = Bitfield(
            bitfieldMessage.bitfield, BitOrder.LITTLE_ENDIAN, bitfield.piecesTotal
        )
        pieceStatistics.addBitfield(peer, peerBitfield)
    }

    private fun consume(have: Have, context: MessageContext) {
        val peer = context.peer
        pieceStatistics.addPiece(peer, have.pieceIndex)
    }
}
