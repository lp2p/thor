package tech.lp2p.loki.torrent

import java.util.Objects

data class Key(val pieceIndex: Int, val offset: Int, val length: Int) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val key = other as Key
        return offset == key.offset && length == key.length && pieceIndex == key.pieceIndex
    }

    override fun hashCode(): Int {
        return Objects.hash(pieceIndex, offset, length)
    }
}
