package tech.lp2p.loki.torrent

import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Type

interface MessageConsumer {
    fun consumedType(): Type

    fun consume(message: Message, messageContext: MessageContext)
}
