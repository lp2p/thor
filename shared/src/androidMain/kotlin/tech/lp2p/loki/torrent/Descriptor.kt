package tech.lp2p.loki.torrent

import kotlinx.atomicfu.atomic
import tech.lp2p.loki.data.DataDescriptor

class Descriptor internal constructor() {
    private val active = atomic(false)

    // !! this can be null in case with magnets (and in the beginning of processing) !!
    @Volatile
    var dataDescriptor: DataDescriptor? = null

    fun isActive(): Boolean {
        return active.value
    }

    fun start() {
        active.value = true
    }

    fun stop() {
        active.value = false
    }
}
