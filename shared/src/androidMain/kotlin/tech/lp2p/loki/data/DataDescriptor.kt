package tech.lp2p.loki.data

import tech.lp2p.loki.TRANSFER_BLOCK_SIZE
import tech.lp2p.loki.debug
import tech.lp2p.loki.torrent.Torrent
import tech.lp2p.loki.torrent.TorrentFile
import kotlin.math.ceil
import kotlin.math.min

class DataDescriptor(private val storage: Storage, torrent: Torrent) :
    AutoCloseable {
    var chunkDescriptors: List<ChunkDescriptor>
    var bitfield: Bitfield
    private var filesForPieces: Map<Int, List<TorrentFile>>
    private var storageUnits: Set<StorageUnit>? = null

    init {
        var transferBlockSize = TRANSFER_BLOCK_SIZE.toLong()
        val files = torrent.files

        val totalSize = torrent.size
        val chunkSize = torrent.chunkSize

        if (transferBlockSize > chunkSize) {
            transferBlockSize = chunkSize
        }

        val chunksTotal = ceil((totalSize / chunkSize).toDouble()).toInt()
        val filesForPieces: MutableMap<Int, List<TorrentFile>> =
            HashMap((chunksTotal / 0.75).toInt() + 1)
        val chunks: MutableList<ChunkDescriptor> = ArrayList(chunksTotal + 1)

        val chunkHashes: List<ByteArray> = torrent.chunkHashes

        val storageUnitsToFilesMap: MutableMap<StorageUnit, TorrentFile> =
            LinkedHashMap((files.size / 0.75).toInt() + 1)
        files.forEach { f: TorrentFile ->
            storageUnitsToFilesMap[storage.getUnit(f)] = f
        }

        // filter out empty files (and create them at once)
        val nonEmptyStorageUnits: MutableList<StorageUnit> = ArrayList()
        for (unit in storageUnitsToFilesMap.keys) {
            if (unit.capacity() > 0) {
                nonEmptyStorageUnits.add(unit)
            }
        }

        if (nonEmptyStorageUnits.isNotEmpty()) {
            val limitInLastUnit = nonEmptyStorageUnits[nonEmptyStorageUnits.size - 1].capacity()
            val data = createReadWriteDataRange(
                nonEmptyStorageUnits, 0, limitInLastUnit
            )

            var off: Long
            var lim: Long
            var remaining = totalSize
            while (remaining > 0) {
                val index = chunks.size
                val hash = chunkHashes[index]
                off = index * chunkSize
                lim = min(chunkSize.toDouble(), remaining.toDouble()).toLong()

                val subrange = data.getSubrange(off, lim)

                val chunkFiles: MutableList<TorrentFile> = ArrayList()
                subrange.visitUnits(object : DataRangeVisitor {
                    override fun visitUnit(unit: StorageUnit, off: Long, lim: Long) {
                        chunkFiles.add(
                            storageUnitsToFilesMap[unit]!!
                        )
                    }
                })
                filesForPieces[chunks.size] = chunkFiles


                chunks.add(buildChunkDescriptor(subrange, transferBlockSize, hash))

                remaining -= chunkSize
            }
        }

        this.bitfield = storage.verifiedPieces(chunks.size)

        this.chunkDescriptors = chunks
        this.storageUnits = storageUnitsToFilesMap.keys
        this.filesForPieces = filesForPieces
    }

    fun getFilesForPiece(pieceIndex: Int): List<TorrentFile> {
        require(!(pieceIndex < 0 || pieceIndex >= bitfield.piecesTotal)) {
            "Invalid piece index: " + pieceIndex +
                    ", expected 0.." + bitfield.piecesTotal
        }
        return filesForPieces[pieceIndex]!!
    }

    override fun close() {
        storage.close()
        storageUnits!!.forEach { unit: StorageUnit ->
            try {
                unit.close()
            } catch (_: Exception) {
                debug("DataDescriptor", "Failed to close storage unit: $unit")
            }
        }
    }

}
