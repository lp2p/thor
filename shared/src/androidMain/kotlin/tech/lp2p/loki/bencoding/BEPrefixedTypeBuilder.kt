package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

abstract class BEPrefixedTypeBuilder : BEObjectBuilder {
    private val buf = Buffer()
    private var receivedPrefix = false
    private var receivedEOF = false

    override fun accept(b: Int): Boolean {
        return accept(b, true)
    }

    // work-around for duplicate logging of received bytes
    // when BEPrefixTypeBuilder.accept(int) is called by itself
    // -- descendants should use this method instead
    fun accept(b: Int, shouldLog: Boolean): Boolean {
        if (receivedEOF) {
            return false
        }

        if (shouldLog) {
            buf.writeByte(b.toByte())
        }

        if (!receivedPrefix) {
            val type = type()
            if (b == getPrefixForType(type).code) {
                receivedPrefix = true
                return true
            } else {
                throw IllegalArgumentException(
                    ("Invalid prefix for type " + type.name.lowercase()
                            + " (as ASCII char): " + b.toChar())
                )
            }
        }

        if (b == EOF.code && acceptEOF()) {
            receivedEOF = true
            return true
        }

        return doAccept(b)
    }

    override fun build(): BEObject {
        check(receivedPrefix) { "Can't build " + type().name.lowercase() + " -- no content" }
        check(receivedEOF) { "Can't build " + type().name.lowercase() + " -- content was not terminated" }
        return doBuild(buf.readByteArray())
    }

    protected abstract fun doAccept(b: Int): Boolean

    protected abstract fun doBuild(content: ByteArray): BEObject

    protected abstract fun acceptEOF(): Boolean
}
