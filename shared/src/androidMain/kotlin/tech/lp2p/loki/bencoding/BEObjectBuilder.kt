package tech.lp2p.loki.bencoding

interface BEObjectBuilder {
    fun accept(b: Int): Boolean

    fun build(): BEObject

    fun type(): BEType
}
