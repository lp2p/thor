package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer


/**
 * BEncoding parser. Should be closed when the source is processed.
 */
data class BEParser(val type: BEType, val scanner: Scanner) {
    /**
     * Read type of the root object of the bencoded document that this parser was created for.
     */
    fun readType(): BEType {
        return type
    }

    /**
     * Try to read the document's root object as a bencoded dictionary.
     */
    fun readMap(): BEMap {
        return readMapObject(BEMapBuilder())
    }


    private fun readMapObject(builder: BEMapBuilder): BEMap {
        check(this.type == BEType.MAP) {
            "Can't read " + BEType.MAP.name.lowercase() +
                    " from: " + type.name.lowercase()
        }
        // relying on the default constructor being present
        return scanner.readMapObject(builder)
    }


}

const val DELIMITER: Char = ':'
const val EOF: Char = 'e'
const val INTEGER_PREFIX: Char = 'i'
const val LIST_PREFIX: Char = 'l'
const val MAP_PREFIX: Char = 'd'

/**
 * Create a parser for the provided bencoded document.
 */
fun createParser(bs: ByteArray): BEParser { // todo buffer
    require(bs.isNotEmpty()) { "Can't parse bytes array: null or empty" }
    val buffer = Buffer()
    buffer.write(bs)
    val scanner = Scanner(buffer)
    val type = getTypeForPrefix(scanner.peek().toChar())
    return BEParser(type, scanner)
}

fun getPrefixForType(type: BEType): Char {
    return when (type) {
        BEType.INTEGER -> INTEGER_PREFIX
        BEType.LIST -> LIST_PREFIX
        BEType.MAP -> MAP_PREFIX
        else -> throw IllegalArgumentException("Unknown type: " + type.name.lowercase())
    }
}

fun getTypeForPrefix(c: Char): BEType {
    if (Character.isDigit(c)) {
        return BEType.STRING
    }
    return when (c) {
        INTEGER_PREFIX -> {
            BEType.INTEGER
        }

        LIST_PREFIX -> {
            BEType.LIST
        }

        MAP_PREFIX -> {
            BEType.MAP
        }

        else -> throw IllegalStateException("Invalid type prefix: $c")
    }
}

fun builderForType(type: BEType): BEObjectBuilder {
    return when (type) {
        BEType.STRING -> {
            BEStringBuilder()
        }

        BEType.INTEGER -> {
            BEIntegerBuilder()
        }

        BEType.LIST -> {
            BEListBuilder()
        }

        BEType.MAP -> {
            BEMapBuilder()
        }

    }
}