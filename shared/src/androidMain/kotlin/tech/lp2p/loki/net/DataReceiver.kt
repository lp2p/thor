package tech.lp2p.loki.net

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tech.lp2p.loki.debug
import java.nio.channels.ClosedSelectorException
import java.nio.channels.SelectableChannel
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

class DataReceiver(
    private val scope: CoroutineScope,
    private val selector: Selector
) {
    private val interestOpsUpdates: ConcurrentMap<SelectableChannel, Int> = ConcurrentHashMap()


    @Volatile
    private var shutdown = false

    fun startup() {
        scope.launch {
            while (!shutdown) {
                if (!selector.isOpen) {
                    continue
                }

                try {
                    do {
                        if (!interestOpsUpdates.isEmpty()) {
                            processInterestOpsUpdates()
                        }
                    } while (selector.select(1000) == 0)

                    while (!shutdown) {
                        val selectedKeys = selector.selectedKeys().iterator()
                        while (selectedKeys.hasNext()) {
                            try {
                                // do not remove the key if it hasn't been processed,
                                // we'll try again in the next loop iteration
                                if (processKey(selectedKeys.next())) {
                                    selectedKeys.remove()
                                }
                            } catch (e: ClosedSelectorException) {
                                // selector has been closed, there's no point to continue processing
                                throw e
                            } catch (_: Exception) {
                                selectedKeys.remove()
                            }
                        }
                        if (selector.selectedKeys().isEmpty()) {
                            break
                        }

                        if (!interestOpsUpdates.isEmpty()) {
                            processInterestOpsUpdates()
                        }
                        selector.selectNow()
                    }
                } catch (_: ClosedSelectorException) {
                    continue
                }
            }
        }
    }

    fun registerChannel(channel: SelectableChannel, context: SocketChannelHandler) {
        // use atomic wakeup-and-register to prevent blocking of registration,
        // if selection is resumed before call to register is performed
        // (there is a race between the message receiving loop and current thread)
        wakeupAndRegister(channel, SelectionKey.OP_READ, context)
    }

    fun wakeupAndRegister(channel: SelectableChannel, ops: Int, attachment: Any?) {
        selector.wakeup()
        channel.register(selector, ops, attachment)
    }

    fun unregisterChannel(channel: SelectableChannel) {
        val key = channel.keyFor(selector)
        key.cancel()
    }

    fun activateChannel(channel: SelectableChannel) {
        interestOpsUpdates[channel] = SelectionKey.OP_READ
    }


    private fun processInterestOpsUpdates() {
        val iter: MutableIterator<Map.Entry<SelectableChannel, Int>> =
            interestOpsUpdates.entries.iterator()
        while (iter.hasNext()) {
            val entry = iter.next()
            val channel = entry.key
            val interestOps = entry.value
            try {
                val key = channel.keyFor(selector)
                key.interestOps(interestOps)
            } catch (e: Exception) {
                debug(
                    "DataReceiver", "Failed to set interest ops for channel " +
                            channel + " to " + interestOps, e
                )
            } finally {
                iter.remove()
            }
        }
    }

    fun shutdown() {
        shutdown = true
    }
}

/**
 * @return true, if the key has been processed and can be removed
 */
private fun processKey(key: SelectionKey): Boolean {
    val handler = getHandlerContext(key)
    if (!key.isValid || !key.isReadable) {
        return true
    }
    return handler.read()
}

private fun getHandlerContext(key: SelectionKey): SocketChannelHandler {
    val obj = key.attachment()
    if (obj !is SocketChannelHandler) {
        throw RuntimeException("Unexpected attachment in selection key: $obj")
    }
    return obj
}