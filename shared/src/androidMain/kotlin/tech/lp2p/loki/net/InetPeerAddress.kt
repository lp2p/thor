package tech.lp2p.loki.net


data class InetPeerAddress(val hostname: String, val port: Int) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val that = other as InetPeerAddress
        return port == that.port && hostname == that.hostname
    }

    override fun hashCode(): Int {
        var result = hostname.hashCode()
        result = 31 * result + port
        return result
    }

}
