package tech.lp2p.loki.net

import kotlinx.atomicfu.locks.ReentrantLock
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tech.lp2p.loki.MAX_PEER_CONNECTIONS
import tech.lp2p.loki.PEER_INACTIVITY_THRESHOLD
import tech.lp2p.loki.torrent.TorrentWorker
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import kotlin.concurrent.withLock

internal class PeerConnectionPool(
    private val scope: CoroutineScope,
    private val torrentWorker: TorrentWorker
) {

    private val connections: ConcurrentMap<Peer, PeerConnection> = ConcurrentHashMap()

    fun visitConnections(visitor: (PeerConnection) -> Unit) {
        connections.values.forEach(visitor)
    }

    private val mutex = ReentrantLock()

    fun startup() {
        scope.launch {
            while (true) {
                delay(1000)
                if (connections.count() > 0) {
                    mutex.withLock {
                        visitConnections { connection: PeerConnection ->
                            if (connection.isClosed) {
                                purgeConnection(connection)
                            } else if (System.currentTimeMillis() - connection.getLastActive()
                                >= PEER_INACTIVITY_THRESHOLD
                            ) {
                                purgeConnection(connection)
                            }
                        }
                    }
                }
                delay(1000)
            }
        }
    }

    fun getConnection(key: Peer): PeerConnection? {
        return connections[key]
    }

    fun count(): Int {
        return connections.count()
    }

    fun addConnectionIfAbsent(newConnection: PeerConnection): PeerConnection {
        var existingConnection: PeerConnection? = null


        mutex.withLock {
            if (connections.count() >= MAX_PEER_CONNECTIONS) {
                newConnection.closeQuietly()
            } else {
                val connectionsWithSameAddress = getConnectionsForAddress(newConnection.remotePeer)

                for (connection in connectionsWithSameAddress) {
                    if (connection.remotePeer.port == newConnection.remotePeer.port) {
                        existingConnection = connection
                        break
                    }
                }
                if (existingConnection == null) {
                    check(connections.putIfAbsent(newConnection.remotePeer, newConnection) == null)
                }
            }
        }
        if (existingConnection != null) {
            return existingConnection
        } else {
            torrentWorker.onPeerConnected(newConnection.remotePeer)
            return newConnection
        }
    }


    fun checkDuplicateConnections(peer: Peer) {
        mutex.withLock {
            val connectionsWithSameAddress = getConnectionsForAddress(peer)

            var outgoingConnection: PeerConnection? = null
            var incomingConnection: PeerConnection? = null
            for (connection in connectionsWithSameAddress) {
                if (connection.remotePort == peer.port) {
                    outgoingConnection = connection
                } else if (connection.remotePeer.port == peer.port) {
                    incomingConnection = connection
                }
                if (outgoingConnection != null && incomingConnection != null) {
                    break
                }
            }
            if (outgoingConnection != null && incomingConnection != null) {
                // always prefer to keep outgoing connections

                incomingConnection.closeQuietly()
            }
        }
    }

    private fun getConnectionsForAddress(peer: Peer): List<PeerConnection> {
        val connectionsWithSameAddress: MutableList<PeerConnection> = ArrayList()
        visitConnections { connection: PeerConnection ->
            val connectionPeer = connection.remotePeer
            if (connectionPeer.inetAddress == peer.inetAddress) {
                connectionsWithSameAddress.add(connection)
            }
        }
        return connectionsWithSameAddress
    }

    private fun purgeConnection(connection: PeerConnection) {
        connections.remove(connection.remotePeer, connection)
        connection.closeQuietly()
        torrentWorker.onPeerDisconnected(connection.remotePeer)
    }

    fun shutdown() {
        visitConnections { obj: PeerConnection -> obj.closeQuietly() }
        connections.clear()
    }

}
