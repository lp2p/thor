package tech.lp2p.loki.net

import tech.lp2p.loki.Loki
import tech.lp2p.loki.protocol.BitOrder
import tech.lp2p.loki.protocol.Bitfield
import tech.lp2p.loki.protocol.Handshake

internal data class BitfieldConnectionHandler(val loki: Loki) : HandshakeHandler {
    override fun processIncomingHandshake(connection: PeerConnection, peerHandshake: Handshake) {
        val descriptor = loki.getDescriptor(loki.torrentId)
        if (descriptor != null && descriptor.isActive()
            && descriptor.dataDescriptor != null
        ) {
            val bitfield = descriptor.dataDescriptor!!.bitfield

            if (bitfield.piecesComplete > 0) {
                val bitfieldMessage =
                    Bitfield(
                        bitfield.toByteArray(BitOrder.LITTLE_ENDIAN)
                    )
                connection.postMessage(bitfieldMessage)
            }
        }
    }

    override fun processOutgoingHandshake(handshake: Handshake) {
        // do nothing
    }
}
