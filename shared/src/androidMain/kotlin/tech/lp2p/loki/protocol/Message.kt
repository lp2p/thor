package tech.lp2p.loki.protocol

interface Message {
    val messageId: Byte
    val type: Type
}
