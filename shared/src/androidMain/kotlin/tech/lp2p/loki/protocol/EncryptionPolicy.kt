package tech.lp2p.loki.protocol

enum class EncryptionPolicy {
    REQUIRE_PLAINTEXT,
    PREFER_PLAINTEXT,
    PREFER_ENCRYPTED,
    REQUIRE_ENCRYPTED;

    fun isCompatible(that: EncryptionPolicy): Boolean {
        return if (this == REQUIRE_PLAINTEXT && that == REQUIRE_ENCRYPTED) {
            false
        } else this != REQUIRE_ENCRYPTED || that != REQUIRE_PLAINTEXT
    }
}
