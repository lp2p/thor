package tech.lp2p.loki.protocol

import tech.lp2p.loki.UT_PEX
import tech.lp2p.loki.bencoding.createParser
import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class PeerExchangeMessageHandler : ExtendedMessageHandler {
    override fun supportedTypes(): Collection<Type> =
        setOf(Type.PeerExchange)

    override fun readMessageType(buffer: ByteBufferView): Type {
        return Type.PeerExchange
    }

    override fun decode(peer: Peer, buffer: ByteBufferView): DecodingResult {

        val payload = ByteArray(buffer.remaining())
        buffer.get(payload)
        val parser = createParser(payload)
        val messageContent = parser.readMap()
        val message = parse(messageContent)
        return DecodingResult(messageContent.content!!.size, message)
    }

    override fun encode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {

        val exchange = message as PeerExchange


        val pos = buffer.position()
        try {
            exchange.writeTo(buffer)
            return true
        } catch (throwable: Throwable) {
            debug("ExtendedHandshakeMessageHandler", throwable)
        }
        buffer.position(pos)
        return false
    }

    override fun localTypeId(): Int {
        return 1
    }

    override fun localName(): String {
        return UT_PEX
    }
}
