package tech.lp2p.loki.protocol

import tech.lp2p.loki.data.DataRange

data class Piece(
    val blockRange: DataRange?,
    val pieceIndex: Int,
    val offset: Int,
    val length: Int
) :
    Message {

    override val type: Type
        get() = Type.Piece
    override val messageId: Byte
        get() = StandardBittorrentProtocol.PIECE_ID
}

fun createPiece(pieceIndex: Int, offset: Int, length: Int, blockRange: DataRange): Piece {
    require(!(pieceIndex < 0 || offset < 0 || length <= 0)) {
        "Invalid arguments: piece index (" +
                pieceIndex + "), offset (" + offset + "), block length (" + length + ")"
    }
    return Piece(blockRange, pieceIndex, offset, length)
}

fun createPiece(pieceIndex: Int, offset: Int, length: Int): Piece {
    require(!(pieceIndex < 0 || offset < 0 || length <= 0)) {
        "Invalid arguments: piece index (" +
                pieceIndex + "), offset (" + offset + "), block length (" + length + ")"
    }
    return Piece(null, pieceIndex, offset, length)
}