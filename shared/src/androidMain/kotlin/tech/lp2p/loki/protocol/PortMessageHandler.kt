package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class PortMessageHandler : UniqueMessageHandler(Type.Port) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.Port, EXPECTED_PAYLOAD_LENGTH, buffer.remaining())
        return decodePort(buffer)
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val port = message as Port
        return writePort(port.port, buffer)
    }

}


private const val EXPECTED_PAYLOAD_LENGTH = Short.SIZE_BYTES

// port: <len=0003><id=9><listen-port>
private fun writePort(port: Int, buffer: ByteBuffer): Boolean {
    if (port < 0 || port > Short.MAX_VALUE * 2 + 1) {
        throw RuntimeException("Invalid port: $port")
    }
    if (buffer.remaining() < Short.SIZE_BYTES) {
        return false
    }

    buffer.put(getShortBytes(port))
    return true
}

private fun decodePort(buffer: ByteBufferView): DecodingResult {
    var consumed = 0
    var message: Message? = null
    val length = Short.SIZE_BYTES

    var s: Short?
    if ((readShort(buffer).also { s = it }) != null) {
        val port = s!!.toInt() and 0x0000FFFF
        message = Port(port)
        consumed = length
    }

    return DecodingResult(consumed, message)
}