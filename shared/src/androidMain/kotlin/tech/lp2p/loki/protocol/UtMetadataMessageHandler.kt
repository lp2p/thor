package tech.lp2p.loki.protocol

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.loki.UT_METADATA
import tech.lp2p.loki.bencoding.BEInteger
import tech.lp2p.loki.bencoding.BEMap
import tech.lp2p.loki.bencoding.BEObject
import tech.lp2p.loki.bencoding.createParser
import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.Peer
import java.math.BigInteger
import java.nio.ByteBuffer

class UtMetadataMessageHandler : ExtendedMessageHandler {
    override fun supportedTypes(): Collection<Type> = setOf(
        Type.UtMetadata
    )

    override fun encode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        debug("UT UtMetadataMessageHandler", "encode")
        val utMetadata = message as UtMetadata

        return encodeMetadata(utMetadata, buffer)
    }

    override fun decode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        debug("UT UtMetadataMessageHandler", "decode")
        val payload = ByteArray(buffer.remaining()) // todo
        buffer.get(payload)
        return decodeMetadata(payload)
    }

    override fun readMessageType(buffer: ByteBufferView): Type {
        return Type.UtMetadata
    }

    override fun localTypeId(): Int {
        return 2
    }

    override fun localName(): String {
        return UT_METADATA
    }
}

fun decodeMetadata(payload: ByteArray): DecodingResult { // todo should be ByteBuffer
    val parser = createParser(payload)
    val m = parser.readMap()
    val length = m.content!!.size
    val messageType = getMessageType(m)
    val pieceIndex = getPieceIndex(m)
    when (messageType) {
        MetaType.REQUEST -> {
            return DecodingResult(length, request(pieceIndex))
        }

        MetaType.DATA -> {
            val data = payload.copyOfRange(length, payload.size)
            return DecodingResult(payload.size, data(pieceIndex, getTotalSize(m), data))
        }

        MetaType.REJECT -> {
            return DecodingResult(length, reject(pieceIndex))
        }
    }
}

fun encodeMetadata(utMetadata: UtMetadata, buffer: ByteBuffer): Boolean {
    val pos = buffer.position()
    try {
        writeMessage(utMetadata, buffer)
        return true
    } catch (throwable: Throwable) {
        debug("ExtendedHandshakeMessageHandler", throwable)
    }
    buffer.position(pos)
    return false
}

private fun writeMessage(message: UtMetadata, out: ByteBuffer) {
    val m = BEMap(null, object : HashMap<String, BEObject>() {
        init {
            put(
                messageTypeField(), BEInteger(
                    BigInteger.valueOf(message.metaType.id.toLong())
                )
            )
            put(
                pieceIndexField(), BEInteger(
                    BigInteger.valueOf(message.pieceIndex.toLong())
                )
            )
            if (message.totalSize > 0) {
                put(
                    totalSizeField(), BEInteger(
                        BigInteger.valueOf(message.totalSize.toLong())
                    )
                )
            }
        }
    })
    val buffer = Buffer()
    m.writeTo(buffer)
    out.put(buffer.readByteArray())
    if (message.data.isNotEmpty()) {
        out.put(message.data)
    }
}

private fun getMessageType(m: BEMap): MetaType {
    val type = m.value[messageTypeField()] as BEInteger?
    val typeId = checkNotNull(type).value.toInt()
    return metaTypeForId(typeId)
}

private fun getPieceIndex(m: BEMap): Int {
    return getIntAttribute(pieceIndexField(), m)
}

private fun getTotalSize(m: BEMap): Int {
    return getIntAttribute(totalSizeField(), m)
}

private fun getIntAttribute(name: String, m: BEMap): Int {
    val value = (m.value[name] as BEInteger?)
    checkNotNull(value) { "Message attribute is missing: $name" }
    return value.value.toInt()
}