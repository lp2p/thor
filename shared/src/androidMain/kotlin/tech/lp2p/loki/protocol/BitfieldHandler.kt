package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class BitfieldHandler : UniqueMessageHandler(Type.Bitfield) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        return decodeBitfield(buffer, buffer.remaining())
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val bitfield = message as Bitfield
        if (buffer.remaining() < bitfield.bitfield.size) {
            return false
        }
        buffer.put(bitfield.bitfield)
        return true
    }
}

// bitfield: <len=0001+X><id=5><bitfield>
private fun decodeBitfield(
    buffer: ByteBufferView,
    length: Int
): DecodingResult {
    var consumed = 0
    var message: Message? = null
    if (buffer.remaining() >= length) {
        val bitfield = ByteArray(length)
        buffer.get(bitfield)
        message = Bitfield(bitfield)
        consumed = length
    }

    return DecodingResult(consumed, message)
}