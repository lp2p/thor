package tech.lp2p.loki.protocol


data class Have(val pieceIndex: Int) : Message {
    override val messageId: Byte
        get() = StandardBittorrentProtocol.HAVE_ID
    override val type: Type
        get() = Type.Have

    init {
        require(pieceIndex >= 0) { "Illegal argument: piece index ($pieceIndex)" }
    }
}
