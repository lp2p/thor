package tech.lp2p.loki.protocol

import tech.lp2p.loki.bencoding.BEInteger
import tech.lp2p.loki.bencoding.BEMap
import tech.lp2p.loki.bencoding.BEObject
import tech.lp2p.loki.net.MESSAGE_TYPE_MAPPING_KEY
import tech.lp2p.loki.net.TCPPORT_PROPERTY

/**
 * Extended handshake is sent during connection initialization procedure
 * by peers that support BEP-10: Extension Protocol.
 * It contains a dictionary of supported extended message types with
 * their corresponding numeric IDs, as well as any additional information,
 * that is specific to concrete BitTorrent clients and BEPs,
 * that utilize extended messaging.
 *
 */
data class ExtendedHandshake(val data: Map<String, BEObject>) : ExtendedMessage {
    override val type: Type
        get() = Type.ExtendedHandshake

    /**
     * @return Set of message type names, that are specified
     * in this handshake's message type mapping.
     */
    var supportedMessageTypes: Set<String>

    init {
        val supportedMessageTypes = data[MESSAGE_TYPE_MAPPING_KEY] as BEMap?
        if (supportedMessageTypes != null) {
            this.supportedMessageTypes = supportedMessageTypes.value.keys.toSet()
        } else {
            this.supportedMessageTypes = emptySet()
        }
    }

    val port: BEInteger?
        /**
         * @return TCP port or null, if absent in message data
         */
        get() = data[TCPPORT_PROPERTY] as BEInteger?


    override fun toString(): String {
        return "[ExtendedHandshake] supported messages {$supportedMessageTypes}, data {$data}"
    }
}
