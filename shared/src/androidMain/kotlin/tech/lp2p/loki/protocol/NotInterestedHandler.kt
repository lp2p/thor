package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class NotInterestedHandler : UniqueMessageHandler(Type.NotInterested) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.NotInterested, 0, buffer.remaining())
        return DecodingResult(0, notInterested())
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        return true
    }
}
