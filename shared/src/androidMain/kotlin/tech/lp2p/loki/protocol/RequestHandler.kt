package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class RequestHandler : UniqueMessageHandler(Type.Request) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.Request, 12, buffer.remaining())
        return decodeRequest(buffer)
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val request = message as Request
        return writeRequest(request.pieceIndex, request.offset, request.length, buffer)
    }

}

// request: <len=0013><id=6><index><begin><length>
private fun writeRequest(
    pieceIndex: Int,
    offset: Int,
    length: Int,
    buffer: ByteBuffer
): Boolean {
    require(!(pieceIndex < 0 || offset < 0 || length <= 0)) {
        ("Invalid arguments: pieceIndex (" + pieceIndex
                + "), offset (" + offset + "), length (" + length + ")")
    }
    if (buffer.remaining() < Integer.BYTES * 3) {
        return false
    }

    buffer.putInt(pieceIndex)
    buffer.putInt(offset)
    buffer.putInt(length)

    return true
}

private fun decodeRequest(buffer: ByteBufferView): DecodingResult {
    var consumed = 0
    var message: Message? = null
    val length = Integer.BYTES * 3

    if (buffer.remaining() >= length) {
        val pieceIndex = checkNotNull(readInt(buffer))
        val blockOffset = checkNotNull(readInt(buffer))
        val blockLength = checkNotNull(readInt(buffer))

        message = Request(pieceIndex, blockOffset, blockLength)
        consumed = length
    }

    return DecodingResult(consumed, message)
}