package tech.lp2p.loki.protocol

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.loki.bencoding.BEInteger
import tech.lp2p.loki.bencoding.BEMap
import tech.lp2p.loki.bencoding.BEObject
import tech.lp2p.loki.bencoding.createParser
import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.MESSAGE_TYPE_MAPPING_KEY
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

internal class ExtendedHandshakeMessageHandler : MessageHandler {
    override fun supportedTypes(): Collection<Type> =
        setOf(Type.ExtendedHandshake)

    private val peerTypeMappings: ConcurrentMap<Peer, MutableMap<Int, String>> =
        ConcurrentHashMap()

    override fun readMessageType(buffer: ByteBufferView): Type {
        return Type.ExtendedHandshake
    }

    private fun processTypeMapping(peer: Peer, mappingObj: BEObject?) {
        if (mappingObj == null) {
            return
        }

        require(mappingObj is BEMap) { "Extended message type must be a dictionary." }

        val mapping = mappingObj.value
        if (mapping.isNotEmpty()) {
            // according to BEP-10, peers are only required to send a delta of changes
            // on subsequent handshakes, so we need to store all mappings received from the peer
            // and merge the changes..
            //
            // subsequent handshake messages can be used to enable/disable extensions
            // without restarting the connection
            peerTypeMappings[peer] = mergeMappings(
                peerTypeMappings.getOrDefault(
                    peer,
                    HashMap()
                ), mapping
            )
        }
    }

    fun getPeerTypeMapping(peer: Peer): Map<Int, String> {
        val mapping: Map<Int, String>? = peerTypeMappings[peer]
        return mapping?.toMap() ?: emptyMap()
    }

    override fun decode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        val payload = ByteArray(buffer.remaining())
        buffer.get(payload)

        val parser = createParser(payload)
        val message = parser.readMap()
        val value = message.value
        processTypeMapping(peer, value[MESSAGE_TYPE_MAPPING_KEY])

        return DecodingResult(
            message.content!!.size, ExtendedHandshake(value)
        )
    }

    override fun encode(peer: Peer, message: Message, msg: ByteBuffer): Boolean {
        val extendedHandshake = message as ExtendedHandshake

        val pos = msg.position()
        try {
            val buffer = Buffer()
            BEMap(null, extendedHandshake.data).writeTo(buffer)
            msg.put(buffer.readByteArray())
            return true
        } catch (throwable: Throwable) {
            debug("ExtendedHandshakeMessageHandler", throwable)
        }
        msg.position(pos)
        return false
    }

}

private fun mergeMappings(
    existing: MutableMap<Int, String>,
    changes: Map<String, BEObject>
): MutableMap<Int, String> {
    for ((typeName, value) in changes) {
        val typeId = (value as BEInteger).value.toInt()
        if (typeId == 0) {
            // by setting type ID to 0 peer signals that he has disabled this extension
            val iter = existing.keys.iterator()
            while (iter.hasNext()) {
                val key = iter.next()
                if (typeName == existing[key]) {
                    iter.remove()
                    break
                }
            }
        } else {
            existing[typeId] = typeName
        }
    }
    return existing
}