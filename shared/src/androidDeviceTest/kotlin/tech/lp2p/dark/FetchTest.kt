package tech.lp2p.dark


import kotlinx.coroutines.runBlocking
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FetchTest {
    @Test
    fun testClientServerDownload(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()

        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        var start = System.currentTimeMillis()

        val fid = TestEnv.createContent(halo, 100)


        var end = System.currentTimeMillis()
        println(
            "Time for creating " + ((end - start)) +
                    " [ms]" + " " + fid.size() / 1000000 + " [MB]"
        )


        assertNotNull(fid)
        start = System.currentTimeMillis()

        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        val temp = halo.tempFile()

        val client = newDark()
        val request = createRequest(loopback, fid)

        val channel = client.channel(request)

        SystemFileSystem.sink(temp).buffered().use { sink ->
            channel.transferTo(sink) {}
        }
        client.shutdown()

        val length = SystemFileSystem.metadataOrNull(temp)?.size ?: 0
        assertEquals(length, fid.size())

        end = System.currentTimeMillis()
        println(
            "Time for downloading " + ((end - start) / 1000) +
                    " [s]" + " " + length / 1000000 + " [MB]"
        )
        SystemFileSystem.delete(temp)
        server.shutdown()
        halo.delete()
    }
}
