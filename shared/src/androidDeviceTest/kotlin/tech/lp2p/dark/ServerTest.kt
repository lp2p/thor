package tech.lp2p.dark

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.SPLITTER_SIZE
import tech.lp2p.halo.newHalo
import java.net.InetAddress
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue
import kotlin.test.fail

class ServerTest {
    @Test
    fun fetchDataTest(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        val input = TestEnv.getRandomBytes(10000000) // 10 MB

        val fid = TestEnv.createContent(halo, "random.bin", input)
        assertNotNull(fid)

        val client = newDark()
        val request = createRequest(loopback, fid)
        val data = client.channel(request).readAllBytes()
        assertTrue(input.contentEquals(data))
        client.shutdown()

        server.shutdown()
        halo.delete()
    }


    @Test
    fun reachableTest(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val peeraddrs = TestEnv.peeraddrs(server.peerId(), serverPort)
        assertNotNull(peeraddrs)
        var reachable = peeraddrs.isEmpty()
        for (peeraddr in peeraddrs) {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)

            val reach = inetAddress.isReachable(50)
            if (reach) {
                println("Reachable " + inetAddress.hostAddress)
                reachable = true
            } else {
                println("Not reachable " + inetAddress.hostAddress)
                println("Site Local " + inetAddress.isSiteLocalAddress)
                println("Link Local " + inetAddress.isLinkLocalAddress)
                println("Any Local " + inetAddress.isAnyLocalAddress)
            }
        }

        assertTrue(reachable)
        server.shutdown()
        halo.delete()
    }


    @Test
    fun serverTest(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)
        assertNotNull(loopback)

        val text = "Hallo das ist ein Test"
        val raw = halo.storeText(text)
        assertNotNull(raw)

        val host = server.peerId()
        assertNotNull(host)
        val client = newDark()
        val data = client.fetchData(createRequest(loopback, raw))
        assertEquals(text, String(data))
        client.shutdown()

        server.shutdown()
        halo.delete()
    }


    @Test
    fun multipleClients(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)

        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        val input = TestEnv.getRandomBytes(SPLITTER_SIZE.toInt())

        val raw = halo.storeData(input)
        assertNotNull(raw)


        val finished = atomic(0)
        val instances = 10

        repeat(instances) {
            try {
                val client = newDark()
                val request = createRequest(loopback, raw)
                val output = client.fetchData(request)
                assertTrue(input.contentEquals(output))
                finished.incrementAndGet()
                client.shutdown()
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                fail()
            }
        }

        assertEquals(finished.value.toLong(), instances.toLong())
        server.shutdown()
        halo.delete()
    }


    @Test
    fun multiplePings(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val request = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        val finished = atomic(0)
        val instances = 10

        runBlocking(Dispatchers.IO) {
            repeat(instances) {
                launch {
                    val client = newDark()
                    try {
                        assertTrue(client.reachable(request))
                        finished.incrementAndGet()
                    } catch (throwable: Throwable) {
                        throwable.printStackTrace()
                        fail()
                    } finally {
                        client.shutdown()
                    }
                }
            }
        }

        assertEquals(finished.value.toLong(), instances.toLong())
        server.shutdown()
        halo.delete()
    }
}