package threads.thor

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.Dark
import tech.lp2p.dark.newDark
import tech.lp2p.lite.createPeeraddr
import tech.lp2p.thor.createBookmarks
import tech.lp2p.thor.createDataStore
import tech.lp2p.thor.createPeers
import tech.lp2p.thor.createTasks
import tech.lp2p.thor.data.Bookmarks
import tech.lp2p.thor.data.Tasks
import threads.thor.model.debug
import java.net.InetAddress
import kotlin.time.measureTime

class App : Application() {
    private var datastore: DataStore<Preferences>? = null
    private var tasks: Tasks? = null
    private var dark: Dark? = null
    private var bookmarks: Bookmarks? = null

    fun datastore(): DataStore<Preferences> {
        return datastore!!
    }

    fun tasks(): Tasks {
        return tasks!!
    }

    fun bookmarks(): Bookmarks {
        return bookmarks!!
    }

    fun dark(): Dark {
        return dark!!
    }

    override fun onCreate() {
        super.onCreate()

        val time = measureTime {
            datastore = createDataStore(applicationContext)
            tasks = createTasks(applicationContext)
            var peers = createPeers(applicationContext)
            bookmarks = createBookmarks(applicationContext)
            dark = newDark(
                bootstrap = bootstrap(),
                peerStore = peers
            )
        }
        debug("App", "App started " + time.inWholeMilliseconds)
    }


    fun bootstrap(): List<Peeraddr> {
        // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
        val peeraddrs = mutableListOf<Peeraddr>()
        try {
            peeraddrs.add(
                createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    InetAddress.getByName("104.131.131.82").address, 4001.toUShort()
                )
            )
        } catch (throwable: Throwable) {
            debug("App", throwable)
        }
        return peeraddrs
    }
}