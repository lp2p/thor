package threads.thor.model

import android.content.Context
import android.net.Uri
import threads.thor.R
import java.io.BufferedReader
import java.io.InputStreamReader


private val AD_HOSTS: MutableSet<String> = HashSet()

fun isAd(context: Context, uri: Uri): Boolean {
    initAds(context)
    val host = uri.host
    return isAdHost(host ?: "")
}


private fun initAds(context: Context) {

    if (AD_HOSTS.isEmpty()) {
        val time = kotlin.system.measureTimeMillis {
            BufferedReader(
                InputStreamReader(
                    context.resources.openRawResource(
                        R.raw.pgl_yoyo_org
                    )
                )
            ).use { reader ->
                while (reader.ready()) {
                    val line = reader.readLine()
                    AD_HOSTS.add(line)
                }
            }
        }
        debug("AdBlocker", "Time init Ads $time")
    }
}

private fun isAdHost(host: String): Boolean {
    if (host.isEmpty()) {
        return false
    }
    val index = host.indexOf('.')
    return index >= 0 && (AD_HOSTS.contains(host) ||
            index + 1 < host.length && isAdHost(host.substring(index + 1)))
}

