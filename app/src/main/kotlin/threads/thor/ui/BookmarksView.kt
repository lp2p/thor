package threads.thor.ui

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxState
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import tech.lp2p.thor.data.Bookmark
import threads.thor.R
import threads.thor.model.iconBitmap
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BookmarksView(
    stateModel: StateModel,
    onShowRequest: (Bookmark) -> Unit = {},
) {

    val bookmarks by stateModel.bookmarks().collectAsState(initial = emptyList())
    val threshold = .5f

    Scaffold(
        topBar = {
            Text(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.secondary)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.bookmarks),
                color = MaterialTheme.colorScheme.onSecondary
            )
        }, content = { padding ->
            LazyColumn(
                modifier = Modifier.padding(padding)
            ) {
                items(
                    items = bookmarks,
                    key = { bookmark ->
                        bookmark.id
                    }) { bookmark ->

                    var dismissState: SwipeToDismissBoxState? = null

                    dismissState = rememberSwipeToDismissBoxState(
                        confirmValueChange = { value ->
                            if (value == SwipeToDismissBoxValue.EndToStart
                                && dismissState!!.progress > threshold
                            ) {
                                stateModel.removeBookmark(bookmark)
                                true
                            } else {
                                false
                            }
                        },
                        positionalThreshold = { it * threshold }
                    )


                    SwipeToDismissBox(
                        modifier = Modifier.animateContentSize(),
                        state = dismissState,
                        backgroundContent = {

                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(MaterialTheme.colorScheme.error),
                                contentAlignment = Alignment.CenterEnd

                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Delete,
                                    modifier = Modifier.padding(8.dp),
                                    contentDescription = stringResource(id = android.R.string.cancel),
                                    tint = MaterialTheme.colorScheme.onError
                                )
                            }
                        },
                        enableDismissFromEndToStart = true,
                        enableDismissFromStartToEnd = false,
                    ) {
                        BookmarkItem(bookmark, onShowRequest)
                    }
                }
            }
        }
    )
}


@Composable
fun BookmarkItem(
    bookmark: Bookmark,
    onShowRequest: (Bookmark) -> Unit = {}
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onShowRequest.invoke(bookmark)
            }
            .background(MaterialTheme.colorScheme.surface)
    ) {
        Row(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxWidth()
        ) {
            val bitmap = iconBitmap(bookmark)
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .size(48.dp)
                    .background(
                        color = MaterialTheme.colorScheme.secondaryContainer,
                        shape = RoundedCornerShape(30.dp)
                    )

            ) {
                if (bitmap != null) {
                    Image(
                        bitmap = bitmap.asImageBitmap(),
                        contentDescription = stringResource(id = R.string.bookmark),
                        modifier = Modifier.size(24.dp)
                    )
                } else {
                    Icon(
                        painter = painterResource(id = R.drawable.view),
                        contentDescription = stringResource(R.string.bookmark),
                        modifier = Modifier.size(24.dp)
                    )
                }
            }
            Column(
                modifier = Modifier
                    .padding(16.dp, 0.dp, 16.dp, 0.dp)
                    .weight(1.0f, true)
            ) {
                Text(
                    text = bookmark.title, maxLines = 1, modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium, softWrap = false
                )
                Text(
                    text = bookmark.url, maxLines = 2, modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.labelSmall, softWrap = false
                )
            }
        }
    }
}

