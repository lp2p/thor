package threads.thor.ui

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import threads.thor.R
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InfoView(
    stateModel: StateModel,
    webViewState: WebViewState
) {

    val url = webViewState.lastLoadedUrl
    val title = webViewState.pageTitle
    val icon = webViewState.pageIcon

    var isChecked by remember { mutableStateOf(false) }
    val homepage by stateModel.homepageUri("").collectAsState("")

    homepage.let { page ->
        isChecked = if (page.isBlank()) {
            false
        } else {
            page == cleanUrl(url)
        }
    }


    Scaffold(
        topBar = {
            Text(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.tertiary)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.information),
                color = MaterialTheme.colorScheme.onTertiary,
            )
        }, content = { padding ->
            Column(
                modifier = Modifier
                    .padding(padding)
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {

                Text(
                    text = infoTitle(title, url),
                    style = MaterialTheme.typography.labelLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(32.dp)
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally)
                )

                if (!url.isNullOrBlank()) {
                    val qrCode: Bitmap = bitmap(cleanUrl(url))
                    Image(
                        bitmap = qrCode.asImageBitmap(),
                        contentDescription = stringResource(id = R.string.url_access),
                        modifier = Modifier
                            .size(240.dp)
                            .fillMaxWidth()
                            .align(Alignment.CenterHorizontally)
                    )

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp, 32.dp)
                    ) {
                        Box(
                            contentAlignment = Alignment.Center,
                            modifier = Modifier
                                .size(48.dp)
                                .background(
                                    color = MaterialTheme.colorScheme.secondaryContainer,
                                    shape = RoundedCornerShape(30.dp)
                                )

                        ) {
                            val bitmap = webViewState.pageIcon
                            if (bitmap != null) {
                                Image(
                                    bitmap = bitmap.asImageBitmap(),
                                    contentDescription = stringResource(id = android.R.string.untitled),
                                    modifier = Modifier.size(24.dp)
                                )
                            } else {
                                Icon(
                                    painter = painterResource(id = R.drawable.view),
                                    contentDescription = stringResource(id = android.R.string.untitled),
                                    modifier = Modifier.size(24.dp)
                                )
                            }
                        }
                        Text(
                            text = cleanUrl(url),
                            style = MaterialTheme.typography.labelMedium,
                            textAlign = TextAlign.Start,
                            modifier = Modifier
                                .weight(1.0f, true)
                                .padding(8.dp, 0.dp, 0.dp, 0.dp)
                                .align(Alignment.CenterVertically)
                        )
                    }

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                    ) {
                        Switch(
                            checked = isChecked,
                            onCheckedChange = { isChecked ->

                                if (isChecked) {
                                    stateModel.homePage(
                                        url, title, icon
                                    )
                                } else {
                                    stateModel.resetHomepage()
                                }
                            })
                        Text(
                            text = stringResource(R.string.home_page_set),
                            modifier = Modifier
                                .align(Alignment.CenterVertically)
                                .padding(16.dp, 0.dp, 16.dp, 0.dp)
                        )
                    }
                }

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1.0f)
                )
            }
        }
    )
}