package threads.thor.ui

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.webkit.WebBackForwardList
import android.webkit.WebHistoryItem
import androidx.core.net.toUri
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import tech.lp2p.thor.model.MimeType
import threads.thor.model.getUriTitle


fun transform(list: WebBackForwardList): List<WebHistoryItem> {
    val items: MutableList<WebHistoryItem> = ArrayList(list.size)
    for (i in list.size - 1 downTo 0) {
        items.add(list.getItemAtIndex(i))
    }
    return items
}

fun canBookmark(url: String?): Boolean {
    return if (!url.isNullOrBlank()) {
        !isPnsUriWithFragment(url)
    } else {
        false
    }
}


fun infoTitle(title: String?, url: String?): String {
    return if (title.isNullOrEmpty() || isPnsUriWithFragment(url)) {
        getUriTitle(url)
    } else {
        title
    }
}

private fun isPnsUriWithFragment(uri: Uri): Boolean {
    if (uri.scheme == "pns") {
        val fragment = uri.fragment
        return !fragment.isNullOrBlank()
    }
    return false
}

private fun isPnsUriWithFragment(url: String?): Boolean {
    if (!url.isNullOrBlank()) {
        val uri = url.toUri()
        return isPnsUriWithFragment(uri)
    }
    return false
}

fun bitmap(url: String): Bitmap {
    val bitMatrix = QRCodeWriter().encode(
        url, BarcodeFormat.QR_CODE, 250, 250
    )
    return createBitmap(bitMatrix)
}

private fun createBitmap(matrix: BitMatrix): Bitmap {
    val width = matrix.width
    val height = matrix.height
    val pixels = IntArray(width * height)
    for (y in 0 until height) {
        val offset = y * width
        for (x in 0 until width) {
            pixels[offset + x] = if (matrix[x, y]) -0x1000000 else -0x1
        }
    }

    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
    return bitmap
}

fun cleanUrl(url: String?): String {
    if (url.isNullOrEmpty()) {
        return ""
    }
    val uri = url.toUri()
    if (uri.scheme == "pns") {
        return Uri.Builder().scheme(uri.scheme).authority(uri.authority).build().toString()
    }
    return uri.toString()
}

fun intentUri(intent: Intent): Uri? {
    val action = intent.action
    if (Intent.ACTION_VIEW == action) {
        val uri = intent.data
        if (uri != null) {
            return uri
        }
    }
    if (Intent.ACTION_SEND == action) {
        if (intent.type == MimeType.PLAIN_MIME_TYPE.name) {
            val query = intent.getStringExtra(Intent.EXTRA_TEXT)

            if (!query.isNullOrEmpty()) {
                return query.toUri()
            }
        }
    }
    return null
}