package threads.thor.work

import android.content.Context
import android.os.Environment
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.io.Buffer
import kotlinx.io.RawSink
import kotlinx.io.buffered
import kotlinx.io.files.Path
import kotlinx.io.readByteArray
import tech.lp2p.halo.newHalo
import threads.thor.App
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.debug
import threads.thor.model.downloadsUri
import threads.thor.model.mimeType
import java.io.File
import java.util.UUID
import java.util.concurrent.atomic.AtomicLong


class DownloadContentWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    override suspend fun doWork(): Result {

        val taskId = inputData.getLong(TID, 0)
        val app = applicationContext as App
        val tasks = app.tasks()
        val task = tasks.tasksDao().task(taskId)

        tasks.tasksDao().active(taskId)
        withContext(Dispatchers.IO) {
            try {
                val cacheFile = File(applicationContext.cacheDir, "task_$taskId")
                if (!cacheFile.isDirectory && !cacheFile.exists()) {
                    val success = cacheFile.mkdir()
                    require(success) { "Could not create directory" }
                }
                val halo = newHalo(Path(cacheFile.absolutePath))
                val dark = app.dark()

                val request = task.uri
                val node = dark.info(request)
                val name = node.name()
                val size = node.size()

                val mimeType = mimeType(name)

                val targetUri = downloadsUri(
                    applicationContext, mimeType, name,
                    Environment.DIRECTORY_DOWNLOADS
                )
                checkNotNull(targetUri)
                val contentResolver = applicationContext.contentResolver

                val read = AtomicLong(0L)


                try {
                    contentResolver.openOutputStream(targetUri).use { os ->
                        checkNotNull(os)

                        object : RawSink {
                            override fun write(source: Buffer, byteCount: Long) {
                                os.write(source.readByteArray(byteCount.toInt()))
                            }

                            override fun flush() {

                            }

                            override fun close() {
                                os.close()
                            }

                        }.buffered().use { sink ->

                            var remember = 0
                            dark.channel(request, halo).transferTo(sink) { n ->

                                val value = read.addAndGet(n.toLong())
                                if (size > 0) {
                                    val percent = ((value * 100.0f) / size).toInt()
                                    if (percent > remember) {
                                        remember = percent
                                        launch {
                                            tasks.tasksDao().progress(taskId, percent / 100.0f)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (throwable: Throwable) {
                    contentResolver.delete(targetUri, null, null)
                    throw throwable
                }

                tasks.tasksDao().finished(taskId, targetUri.toString())

                halo.delete()
            } catch (throwable: Throwable) {
                debug(TAG, throwable)
            }
        }

        if (isStopped) {
            CancelWorker.cancel(applicationContext, taskId)
            return Result.retry()
        }
        return Result.success()
    }


    companion object {
        private val TAG: String = DownloadContentWorker::class.java.simpleName

        private const val TID = "tid"
        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val data = Data.Builder().putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<DownloadContentWorker>()
                .addTag(DOWNLOADS_TAG)
                .setInputData(data.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun download(context: Context, taskId: Long): UUID {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                taskId.toString(), ExistingWorkPolicy.KEEP, work
            )
            return work.id
        }
    }
}
