package threads.thor.work

import android.content.Context
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tech.lp2p.loki.Progress
import tech.lp2p.loki.downloadMagnetUri
import tech.lp2p.loki.parseMagnetUri
import threads.thor.App
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.debug
import threads.thor.model.uploadDirectory
import java.io.File
import java.util.Stack
import java.util.UUID

class DownloadMagnetWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {

    override suspend fun doWork(): Result {
        val taskId = inputData.getLong(TID, 0)
        val app = applicationContext as App
        val tasks = app.tasks()

        return try {
            tasks.tasksDao().work(taskId, id.toString())
            tasks.tasksDao().active(taskId)

            withContext(Dispatchers.IO) {
                val task = tasks.tasksDao().task(taskId)

                val magnetUri = parseMagnetUri(task.uri)

                val dataDir = File(applicationContext.cacheDir, task.name)
                if (!dataDir.isDirectory && !dataDir.exists()) {
                    val success = dataDir.mkdir()
                    require(success) { "Could not create directory" }
                }
                downloadMagnetUri(
                    dataDir,
                    magnetUri,
                    1000,
                    object : Progress {
                        override suspend fun progress(
                            piecesCompleted: Int,
                            piecesTotal: Int
                        ) {
                            if (piecesTotal > 0 && piecesCompleted >= 0) {
                                val progress = (piecesCompleted * 1.0f) / (piecesTotal * 1.0f)

                                debug(TAG, " pieces : $piecesCompleted/$piecesTotal")
                                tasks.tasksDao().progress(taskId, progress)
                            }
                        }
                    }
                )

                if (!isStopped) {
                    val dirs = Stack<String>()
                    uploadDirectory(applicationContext, tasks, taskId, dataDir, dirs, id)
                    dataDir.deleteRecursively()
                }
            }

            if (isStopped) {
                CancelWorker.cancel(applicationContext, taskId)
                return Result.retry()
            } else {
                return Result.success()
            }
        } catch (throwable: Throwable) {
            debug("DownloadMagnetWorker", throwable)
            CancelWorker.cancel(applicationContext, taskId)
            return Result.failure()
        }
    }


    companion object {
        private val TAG: String = DownloadMagnetWorker::class.java.simpleName
        private const val TID = "tid"
        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val builder: Constraints.Builder = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)

            val data: Data.Builder = Data.Builder()
            data.putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<DownloadMagnetWorker>()
                .addTag(DOWNLOADS_TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun download(context: Context, taskId: Long): UUID {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                taskId.toString(),
                ExistingWorkPolicy.KEEP, work
            )
            return work.id
        }
    }
}
