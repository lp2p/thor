package threads.thor.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import threads.thor.App


class CancelWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {

    override suspend fun doWork(): Result {

        val taskId = inputData.getLong(TID, 0)
        val app = applicationContext as App
        val tasks = app.tasks()
        tasks.tasksDao().cancel(taskId)
        return Result.success()
    }


    companion object {

        private const val TID = "tid"
        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val data = Data.Builder().putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<CancelWorker>()
                .setInputData(data.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun cancel(context: Context, taskId: Long) {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                taskId.toString(), ExistingWorkPolicy.KEEP, work
            )
        }
    }
}
